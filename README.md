
# Itsa Web Service

Aplicación para la plataforma Android que supervisa y controla los seguimientos de los docentes del Instituto Tecnológico Superior de Apatzingán y la administración de eventos y asignaturas de los alumnos de dicho instituto. 


![enter image description here](https://github.com/jgcc/Nextopic/blob/master/images/nextopic_1.gif)

![enter image description here](https://github.com/jgcc/Nextopic/blob/master/images/nextopic_2.gif)

### Todos

- Vista para tabletas.
- Corregir bugs con las notificaciones.
- Soporta landscape y que nada truene :^), si tiene y no truena pero todabia no he echo tantas pruebas en landscape.
- Mas opcines para los docentes o una app separada.
- Opción de poder limpiar la fecha despues de espeficar.
- Poder administrar tu calificaciones por materia y unidad, ¿nueva vista? posible cambios en la interfaz.
- Widget de horario (quizas).


License
----

The MIT License (MIT)

Copyright (c) 2018 Jose Carabez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
