package com.itsa.nextopic.ui;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import static com.itsa.nextopic.data.source.Entity.HEADER;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */


public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

    private GestureDetector gestureDetector;
    private RecyclerTouchListener.ClickListener listener;

    public RecyclerTouchListener(Context context, final RecyclerView recyclerView , final RecyclerTouchListener.ClickListener listener) {
        this.listener = listener;
        this.gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && listener != null) {
                    int position = recyclerView.getChildAdapterPosition(child);
                    int type = recyclerView.getAdapter().getItemViewType(position);
                    if (type != HEADER) {
                        if (recyclerView.getAdapter() instanceof MultiSelectAdapter) {
                            MultiSelectAdapter adapter = (MultiSelectAdapter) recyclerView.getAdapter();
                            if (!adapter.isMultiSelectMode()) {
                                adapter.activateMultiSelectMode(true);
                                adapter.addSelection(position);
                            }
                        }
                        RecyclerTouchListener.this.listener.onLongClick(child, position);
                    }
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent e) {
        //On Touch event
        View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if (child != null && listener != null && gestureDetector.onTouchEvent(e)) {
            int position = recyclerView.getChildAdapterPosition(child);
            int type = recyclerView.getAdapter().getItemViewType(position);
            if (type != HEADER) {
                if (recyclerView.getAdapter() instanceof MultiSelectAdapter) {
                    MultiSelectAdapter adapter = (MultiSelectAdapter) recyclerView.getAdapter();
                    if (adapter.isMultiSelectMode()) {
                        adapter.addSelection(position);
                    }
                }
                listener.onClick(child, recyclerView.getChildLayoutPosition(child));
            }
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public interface ClickListener {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }
}
