package com.itsa.nextopic.ui;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.itsa.nextopic.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 01/01/18.
 */

public class CustomDayPicker extends AppCompatTextView implements
        DialogInterface.OnMultiChoiceClickListener,
        DialogInterface.OnClickListener {

    private boolean[] selectedDays;
    private AlertDialog.Builder builder;
    private String[] days;
    boolean isSingleChoice;
    private int singleItemSelected;

    public CustomDayPicker(Context context) {
        super(context);
        init(context);
    }

    public CustomDayPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        days = context.getResources().getStringArray(R.array.days_abbreviation);
        selectedDays = new boolean[5];
        isSingleChoice = false;
        singleItemSelected = 0;
        builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(android.R.string.ok, (dialogInterface, which) -> {
            StringBuilder builder = new StringBuilder();
            if (isSingleChoice) {
                builder.append(days[singleItemSelected]);
            } else {
                for (int i = 0; i < selectedDays.length; i++) {
                    if (selectedDays[i]) {
                        builder.append(days[i]);
                        builder.append(", ");
                    }
                }
            }
            if (builder.length() > 0) {
                builder.replace(builder.length() - 2, builder.length(), ".");
                setText(builder.toString());
            } else {
                setText("");
            }
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.dismiss());
        setOnClickListener(v -> {
                if (isSingleChoice) {
                    builder.setSingleChoiceItems(R.array.days, singleItemSelected, this);
                } else {
                    builder.setMultiChoiceItems(R.array.days, selectedDays, CustomDayPicker.this);
                }
                builder.show();
        });
    }

    public void setSingleChoice(boolean singleChoice, int positionSelected) {
        isSingleChoice = singleChoice;
        singleItemSelected = positionSelected;
        setText(days[positionSelected]);
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        selectedDays[which] = isChecked;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        singleItemSelected = i;
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("superState", super.onSaveInstanceState());
        bundle.putBooleanArray("items", selectedDays);
        bundle.putInt("singleItemPosition", singleItemSelected);
        bundle.putBoolean("isSingleChoice", isSingleChoice);
        return bundle;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            selectedDays = bundle.getBooleanArray("items");
            singleItemSelected = bundle.getInt("singleItemPosition");
            isSingleChoice = bundle.getBoolean("isSingleChoice");
            if (isSingleChoice) {
                builder.setSingleChoiceItems(R.array.days, singleItemSelected, this);
            } else {
                builder.setMultiChoiceItems(R.array.days, selectedDays, this);
            }
            onClick(null, 0);
            state = bundle.getParcelable("superState");
        }
        super.onRestoreInstanceState(state);
    }

    public List<Integer> getDays() {
        List<Integer> data = new ArrayList<>();
        if (isSingleChoice) {
            data.add(singleItemSelected);
        } else {
            for (int i = 0; i < selectedDays.length; i++) {
                if (selectedDays[i]) {
                    data.add(i);
                }
            }
        }
        return data;
    }
}
