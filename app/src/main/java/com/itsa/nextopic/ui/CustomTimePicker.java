package com.itsa.nextopic.ui;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public class CustomTimePicker extends AppCompatTextView{

    private TimePickerDialog pickerDialog;
    private Calendar currentTime;
    private DateFormat dateFormat;
    private TimePickerDialog.OnTimeSetListener delegate;

    private TimePickerDialog.OnTimeSetListener listener = (view, hourOfDay, minute) -> {
        currentTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        currentTime.set(Calendar.MINUTE, minute);
        String time = dateFormat.format(currentTime.getTime());
        setText(time);
        if (delegate != null) {
            delegate.onTimeSet(view, hourOfDay, minute);
        }
    };

    public CustomTimePicker(Context context) {
        super(context);
        init(context);
    }

    public CustomTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomTimePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        dateFormat = new SimpleDateFormat("h:mm a", Locale.getDefault());
        currentTime = Calendar.getInstance();
        pickerDialog = new TimePickerDialog(context, listener,
                currentTime.get(Calendar.HOUR_OF_DAY),
                currentTime.get(Calendar.MINUTE),
                false);
        setOnClickListener(v -> pickerDialog.show());
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("superState", super.onSaveInstanceState());
        bundle.putCharSequence("TIME", getText());
        return bundle;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            setText(bundle.getCharSequence("TIME"));
            state = bundle.getParcelable("superState");
        }
        super.onRestoreInstanceState(state);
    }

    public void onTimeSetListener(TimePickerDialog.OnTimeSetListener delegate) {
        this.delegate = delegate;
    }

    public void setTime(Calendar time) {
        if (time != null) {
            currentTime = time;
            pickerDialog.updateTime(currentTime.get(Calendar.HOUR_OF_DAY),
                    currentTime.get(Calendar.MINUTE));
            String sTime = dateFormat.format(currentTime.getTime());
            setText(sTime);
        }
    }

    public Calendar getTime() {
        return currentTime;
    }

}
