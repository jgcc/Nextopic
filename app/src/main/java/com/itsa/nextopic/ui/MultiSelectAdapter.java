package com.itsa.nextopic.ui;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 24/01/18.
 */

public abstract class MultiSelectAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter {

    private ArrayList<Integer> itemsSelected;
    private boolean isSelectMode;

    private static final String STATE_ACTION = "STATE_ACTION";
    private static final String STATE_ITEMS_SELECTED = "STATE_ITEMS_SELECTED";

    protected MultiSelectAdapter() {
        itemsSelected = new ArrayList<>();
        isSelectMode = false;
    }

    public boolean isMultiSelectMode() {
        return isSelectMode;
    }

    void activateMultiSelectMode(boolean activate) {
        if (activate) {
            isSelectMode = true;
        } else {
            isSelectMode = false;
            itemsSelected.clear();
        }
    }

    void addSelection(int position) {
        if (isSelectMode) {
            if (itemsSelected.contains(position)) {
                itemsSelected.remove(itemsSelected.indexOf(position));
                itemSelected(position, false);
            } else {
                itemsSelected.add(position);
                itemSelected(position, true);
            }
            notifyItemChanged(position);
        }
    }

    public int getSelectionCount() {
        return itemsSelected.size();
    }

    public void onSaveInstanceState(Bundle outState) {
        ArrayList<Integer> items = new ArrayList<>(itemsSelected);
        outState.putBoolean(STATE_ACTION, isSelectMode);
        outState.putIntegerArrayList(STATE_ITEMS_SELECTED, items);
        itemsSelected.clear();
    }

    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(STATE_ACTION)) {
                isSelectMode = savedInstanceState.getBoolean(STATE_ACTION);
                itemsSelected.clear();
                List<Integer> restoredItems = savedInstanceState.getIntegerArrayList(STATE_ITEMS_SELECTED);
                itemsSelected.addAll(restoredItems);
                for (int i : itemsSelected) {
                    notifyItemChanged(i);
                }
            }
        }
    }

    public abstract void itemSelected(int position, boolean selected);

    public void clearSelection() {
        for (int i : itemsSelected) {
            itemSelected(i, false);
        }
        itemsSelected.clear();
        activateMultiSelectMode(false);
    }

    public ArrayList<Integer> getSelectedItems() {
        return itemsSelected;
    }
}
