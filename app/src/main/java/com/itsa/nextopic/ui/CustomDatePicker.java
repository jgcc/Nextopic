package com.itsa.nextopic.ui;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import java.util.Calendar;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public class CustomDatePicker extends AppCompatTextView {

    private DatePickerDialog pickerDialog;
    private Calendar currentDate;
    private DatePickerDialog.OnDateSetListener delegate;

    private DatePickerDialog.OnDateSetListener listener = (view, year, monthOfYear, dayOfMonth) -> {
        currentDate.set(year, monthOfYear, dayOfMonth, 0, 0,0);
        String date = String.format("%s/%s/%s",
                dayOfMonth,
                monthOfYear + 1,
                year);
        setText(date);
        if (delegate != null) {
            delegate.onDateSet(view, year, monthOfYear, dayOfMonth);
        }
    };

    public CustomDatePicker(Context context) {
        super(context);
        init(context);
    }

    public CustomDatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomDatePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        currentDate = Calendar.getInstance();
        int year = currentDate.get(Calendar.YEAR);
        int month = currentDate.get(Calendar.MONTH);
        int day = currentDate.get(Calendar.DAY_OF_MONTH);
        pickerDialog = new DatePickerDialog(context, listener, year, month, day);
        setOnClickListener(v -> pickerDialog.show());
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("superState", super.onSaveInstanceState());
        bundle.putCharSequence("DATE", getText());
        return bundle;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            setText(bundle.getCharSequence("DATE"));
            state = bundle.getParcelable("superState");
        }
        super.onRestoreInstanceState(state);
    }

    public void onDateSetListener(DatePickerDialog.OnDateSetListener delegate) {
        this.delegate = delegate;
    }

    public void setDate(Calendar date) {
        if (date != null) {
            currentDate = date;
            int year = date.get(Calendar.YEAR);
            int month = date.get(Calendar.MONTH);
            int day = date.get(Calendar.DAY_OF_MONTH);
            pickerDialog.updateDate(year, month, day);
            String sDate = String.format("%s/%s/%s",
                    day,
                    month + 1,
                    year);
            setText(sDate);
        }
    }

    public Calendar getDate() {
        return currentDate;
    }
}
