package com.itsa.nextopic.schedule;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.ContentResolverCompat;
import android.support.v4.os.CancellationSignal;
import android.support.v4.os.OperationCanceledException;
import android.util.SparseArray;

import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.data.source.local.NextopicContract;
import com.itsa.nextopic.entities.ScheduleDay;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 26/01/18.
 */
public class ScheduleLoader extends AsyncTaskLoader<SparseArray<List<Entity>>> {

    private final ForceLoadContentObserver observer;
    private CancellationSignal cancellationSignal;
    private SparseArray<List<Entity>> cachedData;
    private Cursor cursor;

    public ScheduleLoader(Context context, @NonNull Bundle args) {
        super(context);
        this.observer = new ForceLoadContentObserver();
    }

    @Override
    protected void onStartLoading() {
        if (cachedData != null) {
            deliverResult(cachedData);
        }
        if (takeContentChanged() || cachedData == null) {
            forceLoad();
        }
    }

    @Override
    public SparseArray<List<Entity>> loadInBackground() {
        SparseArray<List<Entity>> dataSet = new SparseArray<>();
        synchronized (this) {
            if (isLoadInBackgroundCanceled()) {
                throw new OperationCanceledException();
            }
            cancellationSignal = new CancellationSignal();
        }

        try {
            for (int i = 0; i < 5; i++) {
                List<Entity> subjectList = new ArrayList<>();
                        cursor = ContentResolverCompat.query(getContext().getContentResolver(),
                        NextopicContract.ScheduleEntry.buildUri(),
                        null,
                        NextopicContract.ScheduleEntry.TABLE_NAME+"."+
                        NextopicContract.ScheduleEntry.COLUMN_DAY+"="+i,
                        null,
                        null,
                                cancellationSignal);
                if (cursor != null) {
                    try {
                        cursor.registerContentObserver(observer);
                        while (cursor.moveToNext()) {
                            subjectList.add(ScheduleDay.from(cursor));
                        }
                        dataSet.append(i, subjectList);
                    } catch (RuntimeException ex) {
                        cursor.close();
                        throw ex;
                    }
                }
            }
            return dataSet;
        } finally {
            synchronized (this) {
                cancellationSignal = null;
            }
        }
    }

    @Override
    public void deliverResult(SparseArray<List<Entity>> data) {
        cachedData = data;
        super.deliverResult(data);
    }

    @Override
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            if (cancellationSignal != null) {
                cancellationSignal.cancel();
            }
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    public void onCanceled(@Nullable SparseArray<List<Entity>> data) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }
}
