package com.itsa.nextopic.schedule;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.preference.PreferenceManagerFix;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.itsa.nextopic.R;
import com.itsa.nextopic.data.source.local.NextopicContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class ScheduleSyncService extends IntentService {

    public static final String URL = "http://192.168.1.73:8080/api/v1/schedule?semester=%s&career=%s&turn=%s";
    private static final int NOTIFICATION_SYNC_ID = 666;
    SharedPreferences preferences;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;
    String career, semester, turn;

    public ScheduleSyncService() {
        // name Used to name the worker thread, important only for debugging.
        super("sync-service");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(getApplicationContext(), "SYNC_CHANNEL_ID");
        builder.setContentTitle(getString(R.string.app_name))
                .setProgress(0, 0, true)
                .setContentText(getString(R.string.sync_msg))
                .setSmallIcon(R.drawable.ic_action_download);
        notificationManager.notify(NOTIFICATION_SYNC_ID, builder.build());
        preferences = PreferenceManagerFix.getDefaultSharedPreferences(getApplication());
        career = preferences.getString(getString(R.string.career), "1");
        semester = preferences.getString(getString(R.string.semester), "1");
        turn = preferences.getString(getString(R.string.turn), "morning");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String msg = getString(R.string.sync_finished);
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, String.format(URL, semester, career, turn), new JSONObject(), future, future);
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);
        try {
            JSONObject jsonSchudele = future.get();
            ContentResolver contentResolver = getContentResolver();
            JSONArray jsonWeek = jsonSchudele.getJSONArray("week");
            if (jsonWeek.length() > 0) {
                // Delete prev. schedule records
                contentResolver.delete(NextopicContract.ScheduleEntry.buildUri(), null, null);
            }
            for (int j = 0; j < jsonWeek.length(); j++) {
                JSONObject jsonDay = jsonWeek.getJSONObject(j);
                JSONArray jsonSubjectList = jsonDay.getJSONArray("subjects");
                for (int i = 0; i < jsonSubjectList.length(); i++) {
                    ContentValues values = new ContentValues();
                    values.put(BaseColumns._ID, jsonSubjectList.getJSONObject(i).getString("id"));
                    JSONObject jsonSubject = jsonSubjectList.getJSONObject(i).getJSONObject("subject");
                    values.put(NextopicContract.ScheduleEntry.COLUMN_ID_SUBJECT, jsonSubject.getLong("id"));
                    values.put(NextopicContract.ScheduleEntry.COLUMN_DAY, jsonDay.getInt("day"));
                    values.put(NextopicContract.ScheduleEntry.COLUMN_TIME_START, jsonSubjectList.getJSONObject(i).getString("startHour"));
                    values.put(NextopicContract.ScheduleEntry.COLUMN_TIME_FINISH, jsonSubjectList.getJSONObject(i).getString("finishHour"));
                    values.put(NextopicContract.ScheduleEntry.COLUMN_UUID, jsonSubjectList.getJSONObject(i).getString("uuid"));
                    values.put(NextopicContract.ScheduleEntry.COLUMN_DELETED, false);
                    contentResolver.insert(NextopicContract.ScheduleEntry.buildUri(), values);
                }
            }

        } catch (InterruptedException e) {
            Log.e("InterruptedException", e.getMessage());
        } catch (ExecutionException e) {
            builder.setContentTitle(getString(R.string.sync_error));
            msg = getString(R.string.sync_error_msg);
        } catch (JSONException e) {
            Log.e(" JSONException", e.getMessage());
        } finally {
            builder.setContentText(msg)
                    .setProgress(0,0, false)
                    .setAutoCancel(true);
            notificationManager.notify(NOTIFICATION_SYNC_ID, builder.build());
            preferences.edit()
                    .putBoolean(getString(R.string.sync), true)
                    .apply();
        }
    }
}
