package com.itsa.nextopic.schedule;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itsa.nextopic.R;
import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.entities.ScheduleDay;
import com.itsa.nextopic.ui.MultiSelectAdapter;
import com.itsa.nextopic.ui.RecyclerDiffCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public class SchedulerListAdapter extends MultiSelectAdapter<SchedulerListAdapter.ScheduleHolder> {

    private List<Entity> dataSet;

    SchedulerListAdapter() {
        dataSet = new ArrayList<>();
    }

    @Override
    public ScheduleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_schedule, parent, false);
        return new ScheduleHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder baseHolder, int position) {
        ScheduleDay scheduleDay = (ScheduleDay) dataSet.get(position);
        ScheduleHolder holder = (ScheduleHolder) baseHolder;

        holder.separator.setBackgroundColor(scheduleDay.getColor());

        holder.setSelected(scheduleDay.isSelected());
        holder.textViewSubject.setText(scheduleDay.getSubject());
        holder.textViewStartHour.setText(scheduleDay.getStartTime());
        holder.textViewFinishHour.setText(scheduleDay.getFinishTime());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public long getItemId(int position) {
        return dataSet.get(position).getId();
    }

    void swap(List<Entity> dataSet) {
        final RecyclerDiffCallback diffCallback = new RecyclerDiffCallback(this.dataSet, dataSet);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        this.dataSet.clear();
        this.dataSet.addAll(dataSet);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public void itemSelected(int position, boolean selected) {
        dataSet.get(position).setSelected(selected);
    }

    class ScheduleHolder extends RecyclerView.ViewHolder {

        TextView textViewSubject,
                textViewStartHour,
                textViewFinishHour;
        private final int selectedColor;
        private final Drawable normal;
        LinearLayout separator;

        ScheduleHolder(View itemView) {
            super(itemView);
            selectedColor = ContextCompat.getColor(itemView.getContext(), R.color.selected_color);
            normal = itemView.getBackground();
            textViewSubject = itemView.findViewById(R.id.txtSubject);
            textViewStartHour = itemView.findViewById(R.id.txtStartHour);
            textViewFinishHour = itemView.findViewById(R.id.txtFinishHour);
            separator = itemView.findViewById(R.id.separator);
        }

        void setSelected(boolean selected) {
            if (selected) {
                itemView.setBackgroundColor(selectedColor);
            } else {
                itemView.setBackgroundDrawable(normal);
            }
        }
    }
}
