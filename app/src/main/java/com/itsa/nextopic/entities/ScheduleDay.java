package com.itsa.nextopic.entities;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;

import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.data.source.local.NextopicContract;

import java.util.Observable;
import java.util.UUID;

import static android.provider.BaseColumns._ID;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public class ScheduleDay extends Observable implements Entity {

    private final long id;
    private long id_subject;
    private long id_day;
    private String startTime;
    private String finishTime;
    private final String uuid;
    private boolean selected;
    private String subjectName;
    private final int color;

    public ScheduleDay() {
        id = -1;
        this.uuid = UUID.randomUUID().toString();
        this.id_subject = -1;
        this.id_day = -1;
        this.color = 0;
    }

    private ScheduleDay(long id, long id_subject, long id_day, String subjectName, String startTime, String finishTime, String uuid, int color) {
        this.id = id;
        this.id_subject = id_subject;
        this.id_day = id_day;
        this.subjectName = subjectName;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.uuid = uuid;
        this.color = color;
        selected = false;
    }

    public long getIdSubject() {
        return id_subject;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public String getSubject() {
        return subjectName;
    }

    public long getIdDay() {
        return id_day;
    }

    public int getColor() {
        return color;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void setId_subject(long id_subject) {
        if (this.id_subject < 0) {
            this.id_subject = id_subject;
        } else if (this.id_subject != id_subject) {
            this.id_subject = id_subject;
            setChanged();
            notifyObservers();
        }
    }

    public void setId_day(long id_day) {
        if (this.id_day < 0) {
            this.id_day = id_day;
        } else if (this.id_day != id_day) {
            this.id_day = id_day;
            setChanged();
            notifyObservers();
        }
    }

    public void setStartTime(String startTime) {
        if (this.startTime == null) {
            this.startTime = startTime;
        } else if (!startTime.equals(this.startTime)) {
                this.startTime = startTime;
                setChanged();
                notifyObservers();
        }
    }

    public void setFinishTime(String finishTime) {
        if (this.finishTime == null) {
            this.finishTime = finishTime;
        } else if (!finishTime.equals(this.finishTime)) {
            this.finishTime = finishTime;
            setChanged();
            notifyObservers();
        }
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        if (id > 0) {
            contentValues.put(_ID, id);
        }
        contentValues.put(NextopicContract.ScheduleEntry.COLUMN_ID_SUBJECT, id_subject);
        contentValues.put(NextopicContract.ScheduleEntry.COLUMN_DAY, id_day);
        contentValues.put(NextopicContract.ScheduleEntry.COLUMN_TIME_START, startTime);
        contentValues.put(NextopicContract.ScheduleEntry.COLUMN_TIME_FINISH, finishTime);
        contentValues.put(NextopicContract.ScheduleEntry.COLUMN_UUID, uuid);
        contentValues.put(NextopicContract.ScheduleEntry.COLUMN_DELETED, false);
        return contentValues;
    }

    public static ScheduleDay from(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(_ID));
        long id_subject = cursor.getLong(cursor.getColumnIndex(NextopicContract.ScheduleEntry.COLUMN_ID_SUBJECT));
        long id_day = cursor.getLong(cursor.getColumnIndex(NextopicContract.ScheduleEntry.COLUMN_DAY));
        String subjectName= "";
        try {
            subjectName = cursor.getString(cursor.getColumnIndex(NextopicContract.SubjectEntry.COLUMN_SUBJECTS_NAME));
        } catch (Exception e) {
            Log.d("Error", e.toString());
        }
        String startTime = cursor.getString(cursor.getColumnIndex(NextopicContract.ScheduleEntry.COLUMN_TIME_START));
        String finishTime = cursor.getString(cursor.getColumnIndex(NextopicContract.ScheduleEntry.COLUMN_TIME_FINISH));
        String uuid = cursor.getString(cursor.getColumnIndex(NextopicContract.ScheduleEntry.COLUMN_UUID));
        String color = cursor.getString(cursor.getColumnIndex(NextopicContract.SubjectEntry.COLUMN_COLOR));
        return new ScheduleDay(id, id_subject, id_day, subjectName, startTime, finishTime, uuid, Color.parseColor(color));
    }

    @Override
    public String toString() {
        return "id: " + this.id + "\n" +
        "id_subject: " + this.id_subject + "\n" +
        "id_day: " + this.id_day + "\n" +
        "startTime: " + this.startTime + "\n" +
        "finishTime: " + this.finishTime + "\n" +
        "uuid: " + this.uuid + "\n";
    }


}
