package com.itsa.nextopic.entities;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.ContentValues;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public class Subject {

    private String SubjectName;
    private String SubjectKey;
    private String Teacher;
    private int Credits;
    private int Semester;
    private int Units;
    private int Turn;

    public Subject() {
        init();
    }

    private void init(){
        SubjectName = "";
        SubjectKey = "";
        Teacher = "";
        Credits = 0;
        Semester = 0;
        Units = 0;
        Turn = 0;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubjectKey() {
        return SubjectKey;
    }

    public void setSubjectKey(String subjectKey) {
        SubjectKey = subjectKey;
    }

    public String getTeacher() {
        return Teacher;
    }

    public void setTeacher(String teacher) {
        Teacher = teacher;
    }

    public int getCredits() {
        return Credits;
    }

    public void setCredits(int credits) {
        Credits = credits;
    }

    public void setCredits(String credits) {
        if (credits.isEmpty()) { return; }
        Credits = Integer.parseInt(credits);
    }

    public int getSemester() {
        return Semester;
    }

    public void setSemester(int semester) {
        Semester = semester;
    }

    public void setSemester(String semester) {
        if (semester.isEmpty()) { return; }
        Semester = Integer.parseInt(semester);
    }

    public int getUnits() {
        return Units;
    }

    public void setUnits(int units) {
        Units = units;
    }
    public void setUnits(String units) {
        Units = Integer.parseInt(units);
    }

    public int getTurn() {
        return Turn;
    }

    public void setTurn(int turn) {
        Turn = turn;
    }

    public void setTurn(String turn) {
        if (turn.isEmpty()) { return; }
        Turn = Integer.parseInt(turn);
    }

    public ContentValues getContentValues() {
        //ContentValues mContentValues = new ContentValues();
        /*mContentValues.put(SubjectsContract.COLUMN_SUBJECTS_NAME, SubjectName);
        mContentValues.put(SubjectsContract.COLUMN_SUBJECTS_KEY, SubjectKey);
        //mContentValues.put(SubjectEntry.COLUMN_SUBJECTS_TEACHER, Teacher);
        mContentValues.put(SubjectsContract.COLUMN_SUBJECTS_CREDITS, Credits);
        //mContentValues.put(SubjectEntry.COLUMN_SUBJECTS_SEMESTER, Semester);
        //mContentValues.put(SubjectEntry.COLUMN_SUBJECTS_UNITS, Units);
        //mContentValues.put(SubjectEntry.COLUMN_SUBJECTS_TURN, Turn);*/
        return new ContentValues();
    }

    @Override
    public String toString() {
        /*return ("SubjectName : " + this.SubjectName) + "\n" +
                "SubjectKey : " + this.SubjectKey + "\n" +
                "Teacher : " + this.Teacher + "\n" +
                "Credits : " + this.Credits + "\n" +
                "Semester : " + this.Semester + "\n" +
                "Units : " + this.Units + "\n" +
                "Turn : " + this.Turn + "\n" +
                "mUUID : " + this.mUUID + "\n" +
                "Deleted : " + this.Deleted + "\n";*/
        return this.SubjectName;
    }

}
