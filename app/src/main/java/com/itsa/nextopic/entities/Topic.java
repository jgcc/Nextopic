package com.itsa.nextopic.entities;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import com.itsa.nextopic.Utils;
import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.data.source.local.NextopicContract;

import java.util.Calendar;
import java.util.Observable;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public final class Topic extends Observable implements Entity {

    private long id;
    private long subjectId;
    private String name;
    private int turn;
    private String uuid;
    private Calendar scheduleDate;
    private Calendar realDate;
    private boolean selected;

    private Topic(long id, long subjectId, String name, int turn,
                  String scheduleDate, String realDate, String uuid) {
        this.id = id;
        this.subjectId = subjectId;
        this.name = name;
        this.turn = turn;
        this.uuid = uuid;
        this.selected = false;
        this.scheduleDate = Utils.stringToDate(scheduleDate);
        if (!realDate.isEmpty()) {
            this.realDate = Utils.stringToDate(realDate);
        }
    }

    public Topic(long subjectId, Calendar scheduleDate, int turn) {
        this.subjectId = subjectId;
        this.scheduleDate = scheduleDate;
        this.id = -1;
        this.uuid = UUID.randomUUID().toString();
        this.turn = turn;
        this.name = "";
    }

    @Override
    public long getId() {
        return this.id;
    }

    public String getTheme() {
        return name;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public Calendar getScheduleDate() {
        return scheduleDate;
    }

    public Calendar getRealDate() {
        return realDate;
    }

    public int getTurn() {
        return turn;
    }

    public boolean isSelected() {
        return selected;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        String sDate = "";
        if (scheduleDate != null) {
            sDate = Utils.dateToString(scheduleDate);
        }
        String rDate = "";
        if (realDate != null) {
            rDate = Utils.dateToString(realDate);
        }

        if (id > 0) {
            contentValues.put(BaseColumns._ID, id);
        }

        contentValues.put(NextopicContract.TopicEntry.COLUMN_TOPIC_NAME, name);
        contentValues.put(NextopicContract.TopicEntry.COLUMN_SUBJECT_ID, subjectId);
        contentValues.put(NextopicContract.TopicEntry.COLUMN_SCHEDULE_DATE, sDate);
        contentValues.put(NextopicContract.TopicEntry.COLUMN_REAL_DATE, rDate);
        contentValues.put(NextopicContract.TopicEntry.COLUMN_TURN, turn);
        contentValues.put(NextopicContract.TopicEntry.COLUMN_DELETED, false);
        contentValues.put(NextopicContract.TopicEntry.COLUMN_UUID, uuid);

        return contentValues;
    }

    public void setSubjectId(long subjectId) {
        if (subjectId != this.subjectId) {
            this.subjectId = subjectId;
            setChanged();
            notifyObservers();
        }
    }

    public void setName(String name) {
        if (!name.equals(this.name)) {
            this.name = name;
            setChanged();
            notifyObservers();
        }
    }

    public void setTurn(int turn) {
        if (turn != this.turn) {
            this.turn = turn;
            setChanged();
            notifyObservers();
        }
    }

    public void setScheduleDate(Calendar scheduleDate) {
        if (scheduleDate.equals(this.scheduleDate)) {
            this.scheduleDate = scheduleDate;
            setChanged();
            notifyObservers();
        }
    }

    public void setRealDate(Calendar realDate) {
        this.realDate = realDate;
        setChanged();
        notifyObservers();
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        String sRealDate = realDate != null ? Utils.dateToString(realDate) : "";
        String sScheduleDate = scheduleDate != null ? Utils.dateToString(scheduleDate) : "";
        return "id: " + id + "\n"+
        "subjectId: " + subjectId + "\n"+
        "name: " + name + "\n"+
        "turn: " + turn + "\n"+
        "uuid: " + uuid + "\n"+
        "scheduleDate: " + sScheduleDate + "\n"+
        "realDate: " + sRealDate + "\n";
    }

    @Override
    public int getType() {
        if (realDate != null) {
            final int MAX_DAYS = 7;
            long dif = Math.abs(realDate.getTimeInMillis() - scheduleDate.getTimeInMillis());
            long days = TimeUnit.DAYS.convert(dif, TimeUnit.MILLISECONDS);
            if (days < MAX_DAYS) {
                return  THEME_TYPE_OK;
            } else {
                return THEME_TYPE_ERROR;
            }
        }
        return THEME_TYPE_NORMAL;
    }

    @NonNull
    public static Topic from(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(NextopicContract.TopicEntry._ID));
        String name = cursor.getString(cursor.getColumnIndex(NextopicContract.TopicEntry.COLUMN_TOPIC_NAME));
        int turn = cursor.getInt(cursor.getColumnIndex(NextopicContract.TopicEntry.COLUMN_TURN));
        long subjectId = cursor.getLong(cursor.getColumnIndex(NextopicContract.TopicEntry.COLUMN_SUBJECT_ID));
        String scheduleDate = cursor.getString(cursor.getColumnIndex(NextopicContract.TopicEntry.COLUMN_SCHEDULE_DATE));
        String realDate = cursor.getString(cursor.getColumnIndex(NextopicContract.TopicEntry.COLUMN_REAL_DATE));
        String uuid = cursor.getString(cursor.getColumnIndex(NextopicContract.TopicEntry.COLUMN_UUID));

        return new Topic(id, subjectId, name, turn, scheduleDate, realDate, uuid);
    }
}
