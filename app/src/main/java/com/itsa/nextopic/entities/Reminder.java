package com.itsa.nextopic.entities;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.ContentValues;
import android.database.Cursor;

import com.itsa.nextopic.Utils;
import com.itsa.nextopic.data.source.Entity;

import java.util.Calendar;
import java.util.Observable;
import java.util.UUID;

import static android.provider.BaseColumns._ID;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_DATE;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_DELETED;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_FINISHED;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_NOTES;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_TIME;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_TITLE;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_UUID;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */
public final class Reminder extends Observable implements Entity {

    private long id;
    private String title;
    private String notes;
    private Calendar date;
    private Calendar time;
    private final String uuid;
    private boolean finished;
    private boolean deleted;
    private boolean selected;

    public Reminder() {
        this.id = -1;
        this.uuid = UUID.randomUUID().toString();
        this.notes = "";
    }

    private Reminder(long id, String title, String notes, String date, String time, String uuid, boolean finished) {
        this.id = id;
        this.title = title;
        this.notes = notes;
        this.uuid = uuid;
        this.finished = finished;
        if (!date.isEmpty()) {
            this.date = Utils.stringToDate(date);
        }
        if (!time.isEmpty()) {
            this.time = Utils.stringToTime(time);
        }
        deleted = false;
        selected = false;
    }

    @Override
    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getNotes() {
        return notes;
    }

    public Calendar getDate() {
        return date;
    }

    public Calendar getTime() {
        return time;
    }

    public String getUUID() {
        return uuid;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        String sDate = "";
        String sTime = "";

        if (date != null && time != null) {
            sDate = Utils.dateToString(date);
            sTime = Utils.timeToString(time);
        }

        if (id > 0) {
            contentValues.put(_ID, id);
        }

        contentValues.put(COLUMN_NAME_TITLE, title);
        contentValues.put(COLUMN_NAME_NOTES, notes);
        contentValues.put(COLUMN_NAME_DATE, sDate);
        contentValues.put(COLUMN_NAME_TIME, sTime);
        contentValues.put(COLUMN_NAME_UUID, uuid);
        contentValues.put(COLUMN_NAME_FINISHED, finished);
        contentValues.put(COLUMN_NAME_DELETED, deleted);

        return contentValues;
    }

    public boolean isSelected() {
        return selected;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setTitle(String title) {
        if (!title.equals(this.title)) {
            this.title = title;
            setChanged();
            notifyObservers();
        }
    }

    public void setNotes(String notes) {
        if (!notes.equals(this.notes)) {
            this.notes = notes;
            setChanged();
            setChanged();
            notifyObservers();
        }
    }

    public void setDate(String date) {
        if (!date.isEmpty()) {
            this.date = Utils.stringToDate(date);
            setChanged();
            notifyObservers();
        }
    }

    public void setTime(String time) {
        if (!time.isEmpty()) {
            this.time = Utils.stringToTime(time);
            setChanged();
            notifyObservers();
        }
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        String sDate = "";
        String sTime = "";
        if (this.date != null && this.time != null) {
            sDate = Utils.dateToString(this.date);
            sTime = Utils.timeToString(this.time);
        }
        return ("Id : " + this.id + "\n" +
                "Title : " + this.title + "\n" +
                "Notes : " + this.notes + "\n" +
                "Date : " + sDate + "\n" +
                "Time : " + sTime + "\n" +
                "Finished : " + this.finished + "\n");
    }

    @Override
    public int getType() {
        if (notes.isEmpty() && date == null && time == null) {
            return TITLE_ONLY;
        } else if ( date == null && time == null){
            return TITLE_DESCRIPTION;
        } else if (notes.isEmpty()) {
            return TITLE_ALARM;
        }
        return TITLE_ALL;
    }

    public static Reminder from(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(_ID));
        String uuid = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_UUID));
        String title = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_TITLE));
        String notes = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_NOTES));
        String date = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_DATE));
        String time = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_TIME));
        boolean finished = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_FINISHED)) > 0;
        return new Reminder(id, title, notes, date, time, uuid, finished);
    }

    public void setId(long id) {
        this.id = id;
    }
}
