package com.itsa.nextopic;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.itsa.nextopic.data.source.local.NextopicContract;
import com.itsa.nextopic.entities.Reminder;
import com.itsa.nextopic.reminders.Main_ReminderFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.provider.BaseColumns._ID;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_FINISHED;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_TIME;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_TITLE;

/**
 * ${PACKAGE_NAME}
 * Created by jose carabez on 01/04/2017.
 * Nextopic
 */

public class AlarmReceiver extends BroadcastReceiver {

    AlarmManager alarmManager;
    Intent alarmIntent;

    @Override
    public void onReceive(Context context, Intent intent) {

        ContentResolver contentResolver = context.getContentResolver();
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmIntent = new Intent(context, AlarmReceiver.class);

        if (intent.getAction() != null) {
            // recreate all the alarms if device is reboot
            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                String where = COLUMN_NAME_FINISHED + " = ?" +
                        " AND " +
                        NextopicContract.ReminderEntry.COLUMN_NAME_DATE + " != ''";
                String selectionArgs[] = { "0" };

                Cursor result = contentResolver.query(NextopicContract.ReminderEntry.buildTopicUri(),
                        null,
                        where,
                        selectionArgs,
                        null);
                if(result != null) {
                    while (result.moveToNext()) {
                        Reminder reminder = Reminder.from(result);
                        Calendar date = reminder.getDate();
                        date.set(Calendar.HOUR_OF_DAY, reminder.getTime().get(Calendar.HOUR_OF_DAY));
                        date.set(Calendar.MINUTE, reminder.getTime().get(Calendar.MINUTE));
                        Log.d("A", reminder.toString());
                        createAlarm(context, reminder.getId(), reminder.getTitle(), date);
                    }
                    result.close();
                }
            }
        } else {
            if (intent.getExtras() != null) {
                Bundle bundle = intent.getExtras();

                String reminderTitle = bundle.getString(COLUMN_NAME_TITLE, "NONE");
                String reminderTime = bundle.getString(COLUMN_NAME_TIME, "0:0");
                long reminderID = bundle.getLong(_ID, -1);

                // Update the reminder to finished in the local database
                ContentValues contentValues = new ContentValues();
                contentValues.put(COLUMN_NAME_FINISHED, true);
                contentResolver.update(NextopicContract.ReminderEntry.buildTopicUriWith(reminderID),
                        contentValues,
                        null,
                        null);

                // First letter to uppercase
                reminderTitle = reminderTitle.substring(0, 1).toUpperCase() + reminderTitle.substring(1);
                reminderTitle += " " + context.getString(R.string.adjective) + " " + reminderTime;

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "CH_ID")
                        .setSmallIcon(R.drawable.ic_action_time)
                        .setContentTitle(reminderTitle)
                        .setContentText(context.getString(R.string.app_name))
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);

                // Notify the reminder list whe the notification happen
                Intent notificationUpdate = new Intent(Main_ReminderFragment.NOTIFICATION_RECEIVER);
                notificationUpdate.putExtra(_ID, reminderID);
                LocalBroadcastManager.getInstance(context).sendBroadcast(notificationUpdate);

                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                int notifyID = 1;
                if (notificationManager != null) {
                    notificationManager.notify(notifyID, builder.build());
                }
            }
        }
    }

    public void createAlarm(Context context, long id, String title, Calendar date) {

        // delete prev. alarm
        PendingIntent prevPendingIntent = PendingIntent.getBroadcast(context, (int)id, alarmIntent, 0);
        alarmManager.cancel(prevPendingIntent);

        DateFormat timeFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());

        alarmIntent.putExtra(_ID, id);
        alarmIntent.putExtra(NextopicContract.ReminderEntry.COLUMN_NAME_TITLE, title);
        alarmIntent.putExtra(NextopicContract.ReminderEntry.COLUMN_NAME_TIME, timeFormat.format(date.getTime()));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int)id, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.set(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), pendingIntent);
    }
}
