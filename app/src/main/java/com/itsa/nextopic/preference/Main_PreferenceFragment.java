package com.itsa.nextopic.preference;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.itsa.nextopic.R;
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 24/01/18.
 */

public class Main_PreferenceFragment extends PreferenceFragmentCompat {

    public static Main_PreferenceFragment newInstance() {
        return new Main_PreferenceFragment();
    }

    String prevSemester, prevCareer, prevTurn;

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.topic_preference, rootKey);
    }

    @Override
    public void onResume() {
        super.onResume();
        prevCareer = getPreferenceManager().getSharedPreferences().getString(getString(R.string.career), "1");
        prevSemester = getPreferenceManager().getSharedPreferences().getString(getString(R.string.semester), "1");
        prevTurn = getPreferenceManager().getSharedPreferences().getString(getString(R.string.turn), "morning");

    }

    @Override
    public void onPause() {
        super.onPause();
        String currentSemester = getPreferenceManager().getSharedPreferences().getString(getString(R.string.career), "1");
        String currentCareer = getPreferenceManager().getSharedPreferences().getString(getString(R.string.semester), "1");
        String currentTurn = getPreferenceManager().getSharedPreferences().getString(getString(R.string.turn), "morning");

        if (!currentCareer.equals(prevCareer) || !currentSemester.equals(prevSemester) || !currentTurn.equals(prevTurn)) {
            getPreferenceManager().getSharedPreferences().edit()
                    .putBoolean(getString(R.string.sync), false)
                    .apply();
        }
    }
}
