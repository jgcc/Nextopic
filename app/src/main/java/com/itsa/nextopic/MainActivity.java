package com.itsa.nextopic;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.itsa.nextopic.preference.Main_PreferenceFragment;
import com.itsa.nextopic.reminders.Main_ReminderFragment;
import com.itsa.nextopic.schedule.Main_ScheduleFragment;
import com.itsa.nextopic.topics.Main_TopicsFragment;
import com.itsa.nextopic.topics.SaveTopic_Activity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG_REMINDERS = "TAG_REMINDERS";
    private static final String TAG_TOPICS = "TAG_TOPICS";
    private static final String TAG_SCHEDULES = "TAG_SCHEDULES";
    private static final String TAG_PREFERENCE = "TAG_PREFERENCE";

    public static final String SELECTED_ITEM_INSTANCE_STATE = "arg_selected_item";
    public static final String ACTION_MODE_INSTANCE_STATE = "ACTION_MODE_INSTANCE_STATE";
    public static final String STATE_RECYCLERVIEW = "STATE_RECYCLERVIEW";
    public static final String KEY_DELETE_ITEM_ID = "DELETE_ITEM_ID";
    public static final String KEY_DELETE_ARRAY_ITEMS_ID = "DELETE_ARRAY_ITEMS_ID";
    public static final String KEY_RESTORE_ITEM_ID = "RESTORE_ITEM_ID";
    public static final String KEY_RESTORE_ARRAY_ITEMS_ID = "RESTORE_ARRAY_ITEMS_ID";
    public static final String ACTION_SHOW_RESTORE_MSG = "com.show.msg.restore";
    public static final String ACTION_SHOW_RESTORE_ITEMS_MSG = "com.show.msg.restore.items";

    public static final int REQUEST_NEW_ITEM = 666;
    public static final int REQUEST_UPDATE_ITEM = 999;

    public FloatingActionButton fab;
    private int mSelectedItem;

    private Main_ReminderFragment reminderFragment;
    private Main_ScheduleFragment scheduleFragment;
    private Main_TopicsFragment topicsFragment;
    private Main_PreferenceFragment preferenceFragment;

    private Intent topicsIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_content);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = findViewById(R.id.fab);

        mSelectedItem = savedInstanceState == null ? R.id.Reminders : savedInstanceState.getInt(SELECTED_ITEM_INSTANCE_STATE, R.id.Reminders);

        reminderFragment = (Main_ReminderFragment) getSupportFragmentManager().findFragmentByTag(TAG_REMINDERS);
        scheduleFragment = (Main_ScheduleFragment) getSupportFragmentManager().findFragmentByTag(TAG_SCHEDULES);
        topicsFragment = (Main_TopicsFragment) getSupportFragmentManager().findFragmentByTag(TAG_TOPICS);
        preferenceFragment = (Main_PreferenceFragment) getSupportFragmentManager().findFragmentByTag(TAG_PREFERENCE);

        if (reminderFragment == null) {
            reminderFragment = Main_ReminderFragment.newInstance();
        }
        if(scheduleFragment == null) {
            scheduleFragment = Main_ScheduleFragment.newInstance();
        }
        if (topicsFragment == null) {
            topicsFragment = Main_TopicsFragment.newInstance();
        }
        if (preferenceFragment == null) {
            preferenceFragment = Main_PreferenceFragment.newInstance();
        }


        BottomNavigationView bottomNavigation = findViewById(R.id.navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            mSelectedItem = item.getItemId();
            fab.show();
            switch (item.getItemId()) {
                case R.id.Reminders:
                    displayReminderFragment();
                    return true;
                case R.id.Schedule:
                    displayScheduleFragment();
                    return true;
                case R.id.Topics:
                    displayTopicFragment();
                    return true;
                case R.id.Preference:
                    displayPreferenceFragment();
                    return true;
            }
            return false;
        });

        // Show/hide views
        bottomNavigation.setSelectedItemId(mSelectedItem);
        switch (mSelectedItem) {
            case R.id.Schedule:
                displayScheduleFragment();
                break;
            case R.id.Topics:
                displayTopicFragment();
                break;
            default:
                displayReminderFragment();
                break;
        }

        topicsIntent = new Intent(this, SaveTopic_Activity.class);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SELECTED_ITEM_INSTANCE_STATE, mSelectedItem);
        super.onSaveInstanceState(outState);
    }



    private void displayReminderFragment() {
        if (!reminderFragment.isAdded()) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.replace(R.id.content, reminderFragment, TAG_REMINDERS);
            transaction.commit();
            setTitle(R.string.reminder);
        }
    }

    private void displayScheduleFragment() {
        if (!scheduleFragment.isAdded()) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.replace(R.id.content, scheduleFragment, TAG_SCHEDULES);
            transaction.commit();
            setTitle(R.string.schedule);
            fab.hide();
        }
    }

    private void displayTopicFragment() {
        if (!topicsFragment.isAdded()) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.replace(R.id.content, topicsFragment, TAG_TOPICS);
            transaction.commit();
            setTitle(R.string.topicThemes);
            fab.setOnClickListener(v -> startActivity(topicsIntent));
        }
    }

    private void displayPreferenceFragment() {
        if (!preferenceFragment.isAdded()) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.replace(R.id.content, preferenceFragment, TAG_PREFERENCE);
            transaction.commit();
            setTitle(R.string.preference);
            fab.hide();
        }
    }

}
