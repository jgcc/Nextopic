package com.itsa.nextopic;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManagerFix;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.itsa.nextopic.data.source.AssetDatabaseHelper;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 25/01/18.
 */

public class SetupActivity extends AppCompatActivity {

    private static final String KEY_SETUP = "configured";
    private Intent intent;
    private SharedPreferences preferences;
    private String[] careerId;
    private String[] semesterId;
    private Spinner careerSpinner, semesterSpinner;
    private RadioGroup radioGroupTurn;
    private ProgressBar progressBarSetup;
    private Button buttonSetup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        preferences = PreferenceManagerFix.getDefaultSharedPreferences(this);

        careerSpinner = findViewById(R.id.career_spinner);
        semesterSpinner = findViewById(R.id.semester_spinner);
        progressBarSetup = findViewById(R.id.progressBarSetup);
        radioGroupTurn = findViewById(R.id.radioGroupTurn);

        careerId = getResources().getStringArray(R.array.career_id_array);
        semesterId = getResources().getStringArray(R.array.semester_array_number);

        buttonSetup = findViewById(R.id.btnNext);
        buttonSetup.setOnClickListener(view -> new DataConfig().execute(getApplicationContext()));

        if (preferences.contains(KEY_SETUP)){
            startActivity(intent);
        }
    }

    // TODO: leaks might occur
    private class DataConfig extends AsyncTask<Context, Void, Void> {

        /*@Override
        protected Void doInBackground(String... strings) {
            final String URL = "http://192.168.1.73:8080/export";
            RequestFuture<JSONArray> future = RequestFuture.newFuture();
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, URL, new JSONArray(), future, future);
            RequestQueue queue = Volley.newRequestQueue(SetupActivity.this);
            queue.add(request);
            try {
                JSONArray jsonReticleList = future.get();
                ContentResolver contentResolver = getContentResolver();
                for (int i = 0; i < jsonReticleList.length(); i++) {
                    JSONObject jsonReticle = jsonReticleList.getJSONObject(i);
                    JSONArray jsonSubjectList = jsonReticle.getJSONArray("subjects");
                    for (int j = 0; j < jsonSubjectList.length(); j++) {
                        ContentValues values = new ContentValues();
                        JSONObject jsonSubject = jsonSubjectList.getJSONObject(j);
                        values.put(NextopicContract.SubjectEntry.COLUMN_SUBJECTS_NAME, jsonSubject.getString("name"));
                        values.put(NextopicContract.SubjectEntry.COLUMN_SUBJECTS_KEY, jsonSubject.getString("key"));
                        values.put(NextopicContract.SubjectEntry.COLUMN_SUBJECTS_CREDITS, jsonSubject.getInt("credits"));
                        values.put(NextopicContract.SubjectEntry.COLUMN_COLOR, jsonSubject.getString("color"));
                        values.put(NextopicContract.SubjectEntry.COLUMN_SUBJECTS_HP, jsonSubject.getInt("hp"));
                        values.put(NextopicContract.SubjectEntry.COLUMN_SUBJECTS_HT, jsonSubject.getInt("ht"));
                        values.put(NextopicContract.SubjectEntry.COLUMN_SUBJECTS_SEMESTER_ID, jsonReticle.getInt("semester"));
                        values.put(NextopicContract.SubjectEntry.COLUMN_SUBJECTS_CAREER_ID, jsonReticle.getJSONObject("career").getInt("id"));
                        contentResolver.insert(NextopicContract.SubjectEntry.buildTopicUri(), values);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return future.toString();
        }*/

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            careerSpinner.setEnabled(false);
            semesterSpinner.setEnabled(false);
            radioGroupTurn.setEnabled(false);
            buttonSetup.setEnabled(false);
            progressBarSetup.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Context... contexts) {
            final AssetDatabaseHelper assetDatabaseHelper = new AssetDatabaseHelper(contexts[0]);
            assetDatabaseHelper.openDatabase();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            RadioButton selectedTurn = radioGroupTurn.findViewById(radioGroupTurn.getCheckedRadioButtonId());
            preferences.edit()
                    .putBoolean(KEY_SETUP, true)
                    .putString(getString(R.string.career), careerId[careerSpinner.getSelectedItemPosition()])
                    .putString(getString(R.string.semester), semesterId[semesterSpinner.getSelectedItemPosition()])
                    .putString(getString(R.string.turn), selectedTurn.getTag().toString())
                    .apply();
            startActivity(intent);
        }
    }
}
