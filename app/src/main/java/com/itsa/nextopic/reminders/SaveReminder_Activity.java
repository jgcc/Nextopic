package com.itsa.nextopic.reminders;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;

import com.itsa.nextopic.R;
import com.itsa.nextopic.data.source.local.NextopicContract;
import com.itsa.nextopic.entities.Reminder;
import com.itsa.nextopic.ui.CustomDatePicker;
import com.itsa.nextopic.ui.CustomTimePicker;
import com.itsa.nextopic.ui.ErrorDialog;
import com.itsa.nextopic.ui.SaveDialog;

import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import static android.provider.BaseColumns._ID;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_TIME;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_TITLE;
import static com.itsa.nextopic.reminders.Main_ReminderFragment.NOTIFICATION_CREATE;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */
public class SaveReminder_Activity extends AppCompatActivity implements TextWatcher, Observer {

    private EditText titleEditText, notesEditText;
    private CustomDatePicker datePicker;
    private CustomTimePicker timePicker;

    private Reminder reminder;
    private boolean hasChanged;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_reminder);
        setTitle(R.string.add_reminder);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        titleEditText = findViewById(R.id.txtTitle);
        notesEditText = findViewById(R.id.txtNotes);
        datePicker = findViewById(R.id.datePicker);
        timePicker = findViewById(R.id.timePicker);

        titleEditText.addTextChangedListener(this);
        notesEditText.addTextChangedListener(this);

        datePicker.onDateSetListener((DatePicker view, int year, int month, int dayOfMonth) ->
                reminder.setDate(datePicker.getText().toString())
        );

        timePicker.onTimeSetListener((view, hourOfDay, minute) ->
                reminder.setTime(timePicker.getText().toString())
        );

        if (getIntent().getExtras() != null) {
            populate();
        } else {
            reminder = new Reminder();
        }
        reminder.addObserver(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (hasChanged) {
                    SaveDialog.newInstance(R.string.warning_dialog_msg)
                            .show(getSupportFragmentManager(), "SAVE");
                } else {
                    finish();
                }
                return true;
            case R.id.action_save:
                save();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
    @Override public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {
        if (s == titleEditText.getEditableText()) {
            reminder.setTitle(titleEditText.getText().toString());
        } else if (s == notesEditText.getEditableText()) {
            reminder.setNotes(notesEditText.getText().toString());
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        this.hasChanged = true;
    }


    private void save() {
        String title = titleEditText.getText().toString();
        String time = timePicker.getText().toString();
        String date = datePicker.getText().toString();

        if (title.isEmpty()) {
            titleEditText.setError(getString(R.string.msg_error_empty_title));
            return;
        }

        if ((date.isEmpty() && !time.isEmpty()) || (!date.isEmpty() && time.isEmpty())) {
            time = "";
            date = "";
        }

        Intent responseIntent = new Intent();

        if (!date.isEmpty() && !time.isEmpty()) {
            Calendar today = Calendar.getInstance();
            Calendar selectedDate = Calendar.getInstance();
            selectedDate.set(
                    datePicker.getDate().get(Calendar.YEAR),
                    datePicker.getDate().get(Calendar.MONTH),
                    datePicker.getDate().get(Calendar.DAY_OF_MONTH),
                    timePicker.getTime().get(Calendar.HOUR_OF_DAY),
                    timePicker.getTime().get(Calendar.MINUTE),
                    timePicker.getTime().get(Calendar.SECOND)
            );

            if (selectedDate.before(today)) {
                ErrorDialog
                        .newInstance(R.string.msg_error_invalid_date)
                        .show(getSupportFragmentManager(), "DIALOG");
                return;
            }
            // Return data to create a notification
            responseIntent.putExtra(NOTIFICATION_CREATE, true);
            responseIntent.putExtra(COLUMN_NAME_TITLE, title);
            responseIntent.putExtra(COLUMN_NAME_TIME, selectedDate.getTimeInMillis());
        }

        // Update or insert in the database
        if (getIntent().getExtras() != null) {
            getContentResolver().update(NextopicContract.ReminderEntry.buildTopicUriWith(reminder.getId()), reminder.getContentValues(), null, null);
        } else {
            getContentResolver().insert(NextopicContract.ReminderEntry.buildTopicUri(), reminder.getContentValues());
        }

        if (getIntent().getExtras() != null) {
            responseIntent.putExtra(_ID, reminder.getId());
        } else {
            // Get the id from the last inserted reminder
            String[] projection = {_ID};
            String orderBy = _ID + " DESC LIMIT 1";
            Cursor cursor = getContentResolver().query(NextopicContract.ReminderEntry.buildTopicUri(), projection, null, null, orderBy);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    responseIntent.putExtra(_ID, cursor.getLong(cursor.getColumnIndex(_ID)));
                }
                cursor.close();
            }
        }

        setResult(RESULT_OK, responseIntent);
        finish();
    }

    private void populate() {
        long id = getIntent().getLongExtra(_ID, -1);
        Cursor cursor = getContentResolver().query(NextopicContract.ReminderEntry.buildTopicUriWith(id), null, null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                reminder = Reminder.from(cursor);
                titleEditText.setText(reminder.getTitle());
                notesEditText.setText(reminder.getNotes());
                datePicker.setDate(reminder.getDate());
                timePicker.setTime(reminder.getTime());
            }
            cursor.close();
        }
    }

}