package com.itsa.nextopic.reminders;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.itsa.nextopic.AlarmReceiver;
import com.itsa.nextopic.MainActivity;
import com.itsa.nextopic.R;
import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.data.source.local.NextopicContract;
import com.itsa.nextopic.ui.RecyclerTouchListener;
import com.itsa.nextopic.ui.SwipeMenu;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.provider.BaseColumns._ID;
import static com.itsa.nextopic.MainActivity.ACTION_MODE_INSTANCE_STATE;
import static com.itsa.nextopic.MainActivity.REQUEST_NEW_ITEM;
import static com.itsa.nextopic.MainActivity.REQUEST_UPDATE_ITEM;
import static com.itsa.nextopic.MainActivity.STATE_RECYCLERVIEW;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_DELETED;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_TIME;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_TITLE;

public class Main_ReminderFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<List<Entity>>,
        ActionMode.Callback,
        SearchView.OnQueryTextListener {

    public static final String NOTIFICATION_CREATE = "com.notification.NOTIFICATION_CREATE";
    public static final String NOTIFICATION_RECEIVER = "com.notification.NOTIFICATION_RECEIVER";

    public static final int LOADER_ID = 2393;

    private MainActivity mainActivity;
    private TextView emptyMessage;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ReminderListAdapter adapter;

    private ActionMode actionMode;
    private int statusBarColor;
    private boolean isActionMode;
    private String selectedText;

    private AlarmManager alarmManager;
    private Intent alarmIntent;
    private Intent reminderIntent;

    public static Main_ReminderFragment newInstance() {
        return new Main_ReminderFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            mainActivity = (MainActivity) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO: setHasOptionsMenu(true);
        final View view = getView() != null ? getView() :
                inflater.inflate(R.layout.main_list_reminder, container, false);

        selectedText = getString(R.string.selected);

        alarmManager = (AlarmManager) mainActivity.getSystemService(Context.ALARM_SERVICE);
        alarmIntent = new Intent(getContext(), AlarmReceiver.class);
        reminderIntent = new Intent(getContext(), SaveReminder_Activity.class);

        emptyMessage = view.findViewById(R.id.empty_msg);

        layoutManager = new LinearLayoutManager(mainActivity);
        adapter = new ReminderListAdapter();
        recyclerView = view.findViewById(R.id.reminderList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (adapter.isMultiSelectMode()) {
                    actionMode.setTitle(selectedText + " " + adapter.getSelectionCount());
                    if (adapter.getSelectionCount() == 0) {
                        actionMode.finish();
                    }
                } else {
                    Intent intent = new Intent(getContext(), SaveReminder_Activity.class);
                    intent.putExtra(_ID, adapter.getItemId(position));
                    startActivityForResult(intent, REQUEST_UPDATE_ITEM);
                }
            }

            @Override public void onLongClick(View view, int position) {
                if (adapter.isMultiSelectMode()) {
                    mainActivity.startSupportActionMode(Main_ReminderFragment.this);
                }
            }
        }));

        if (getContext() != null) {
            ItemTouchHelper touchHelper = new ItemTouchHelper(new SwipeMenu(getContext(), position -> {
                deleteItem(adapter.getItemId(position));
                showRestoreMessage(adapter.getItemId(position));
            }));
            touchHelper.attachToRecyclerView(recyclerView);
        }

        if (savedInstanceState != null) {
            isActionMode = savedInstanceState.getBoolean(ACTION_MODE_INSTANCE_STATE);
            adapter.onRestoreInstanceState(savedInstanceState);
            layoutManager.onRestoreInstanceState(savedInstanceState.getParcelable(STATE_RECYCLERVIEW));
        }

        if (isActionMode) {
            mainActivity.startSupportActionMode(Main_ReminderFragment.this);
        }


        mainActivity.fab.setOnClickListener(v -> startActivityForResult(reminderIntent, REQUEST_NEW_ITEM));

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.clearSelection();
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (data.getExtras() != null) {
                Bundle bundle = data.getExtras();
                if (bundle.containsKey(NOTIFICATION_CREATE)) {
                    long id = bundle.getLong(_ID);
                    String title = bundle.getString(COLUMN_NAME_TITLE);
                    Calendar date = Calendar.getInstance();
                    date.setTimeInMillis(bundle.getLong(COLUMN_NAME_TIME));
                    createAlarm(id, title, date);
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        adapter.onSaveInstanceState(outState);
        outState.putBoolean(ACTION_MODE_INSTANCE_STATE, isActionMode);
        outState.putParcelable(STATE_RECYCLERVIEW, layoutManager.onSaveInstanceState());
    }

    @Override
    public Loader<List<Entity>> onCreateLoader(int id, Bundle args) {
        return new ReminderLoader(getContext(), args);
    }

    @Override
    public void onLoadFinished(Loader<List<Entity>> loader, List<Entity> data) {
        if (data != null) {
            if (data.size() > 0) {
                emptyMessage.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adapter.swap(data);
            } else {
                emptyMessage.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Entity>> loader) {
        adapter.swap(Collections.emptyList());
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d("Search", newText);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    // TODO implement search menu
    /*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        searchItem.setShowAsAction(SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | SHOW_AS_ACTION_IF_ROOM);
        searchItem.setActionView(searchView);
        super.onCreateOptionsMenu(menu, inflater);
    }*/

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            statusBarColor = mainActivity.getWindow().getStatusBarColor();
            mainActivity.getWindow().setStatusBarColor(ContextCompat.getColor(mainActivity, R.color.primaryDark_selected_state));
        }
        mode.getMenuInflater().inflate(R.menu.menu_action_list, menu);
        mode.setTitle(getString(R.string.selected) + " " + adapter.getSelectionCount());
        actionMode = mode;
        isActionMode = true;
        mainActivity.fab.hide();
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                long[] itemsToDelete = Stream.of(adapter.getSelectedItems())
                        .mapToLong( i -> adapter.getItemId(i))
                        .toArray();
                deleteItems(itemsToDelete);
                mode.finish();
                return true;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        //return to old color of status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mainActivity.getWindow().setStatusBarColor(statusBarColor);
        }
        adapter.clearSelection();
        actionMode = null;
        isActionMode = false;
        mainActivity.fab.show();
    }

    @Override public boolean onPrepareActionMode(ActionMode mode, Menu menu) { return false; }

    public void createAlarm(long id, String title, Calendar date) {
        // delete prev. alarm
        removeAlarm((int) id);

        DateFormat timeFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());

        alarmIntent.putExtra(_ID, id);
        alarmIntent.putExtra(COLUMN_NAME_TITLE, title);
        alarmIntent.putExtra(NextopicContract.ReminderEntry.COLUMN_NAME_TIME, timeFormat.format(date.getTime()));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), (int)id, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.set(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), pendingIntent);
    }

    public void removeAlarm(int id) {
        PendingIntent prevPendingIntent = PendingIntent.getBroadcast(getContext(), id, alarmIntent, 0);
        alarmManager.cancel(prevPendingIntent);
    }

    public void showRestoreMessage(long id) {
        Snackbar.make(mainActivity.fab, R.string.msg_delete_reminder, Snackbar.LENGTH_LONG)
                .setAction(R.string.undo, v -> restoreItem(id))
                .show();
    }

    public void showRestoreMultipleItems(long[] itemsToRestore) {
        String msg = getResources().getQuantityString(R.plurals.items_to_restore, itemsToRestore.length);
        Snackbar.make(mainActivity.fab, msg, Snackbar.LENGTH_LONG)
                .setAction(R.string.undo, v -> restoreItems(itemsToRestore))
                .show();
    }

    private void restoreItem(long id) {
        if (getContext() != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_NAME_DELETED, false);
            getContext().getContentResolver()
                    .update(NextopicContract.ReminderEntry.buildTopicUriWith(id),
                            contentValues,
                            null,
                            null);
        }
    }

    private void restoreItems(long[] itemsToRestore) {
        for (long id : itemsToRestore) {
            restoreItem(id);
        }
    }

    private void deleteItem(long id) {
        if (getContext() != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_NAME_DELETED, true);
            getContext().getContentResolver()
                    .update(NextopicContract.ReminderEntry.buildTopicUriWith(id),
                            contentValues,
                            null,
                            null);
        }
    }

    private void deleteItems(long[] indexListToDelete) {
        for (long id : indexListToDelete) {
            deleteItem(id);
        }
        showRestoreMultipleItems(indexListToDelete);
    }

}
