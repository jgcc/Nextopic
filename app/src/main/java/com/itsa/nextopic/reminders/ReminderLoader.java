package com.itsa.nextopic.reminders;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.ContentResolverCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.os.CancellationSignal;
import android.support.v4.os.OperationCanceledException;
import android.util.Log;

import com.itsa.nextopic.R;
import com.itsa.nextopic.Utils;
import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.data.source.local.NextopicContract;
import com.itsa.nextopic.entities.Reminder;
import com.itsa.nextopic.entities.ReminderHeader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.itsa.nextopic.MainActivity.ACTION_SHOW_RESTORE_ITEMS_MSG;
import static com.itsa.nextopic.MainActivity.ACTION_SHOW_RESTORE_MSG;
import static com.itsa.nextopic.MainActivity.KEY_DELETE_ARRAY_ITEMS_ID;
import static com.itsa.nextopic.MainActivity.KEY_DELETE_ITEM_ID;
import static com.itsa.nextopic.MainActivity.KEY_RESTORE_ARRAY_ITEMS_ID;
import static com.itsa.nextopic.MainActivity.KEY_RESTORE_ITEM_ID;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_DATE;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_DELETED;
import static com.itsa.nextopic.data.source.local.NextopicContract.ReminderEntry.COLUMN_NAME_FINISHED;

public class ReminderLoader extends AsyncTaskLoader<List<Entity>> {

    private List<Entity> cachedData;
    private final String TITLE_FINISHED;
    private final String TITLE_TODAY;
    private final String TITLE_TOMORROW;
    private final String TITLE_THIS_WEEK;
    private final String TITLE_THIS_MONTH;
    private final String TITLE_OTHERS;

    private Bundle bundle;
    private ContentValues contentValues;

    private Cursor cursor;
    private CancellationSignal cancellationSignal;
    private final ForceLoadContentObserver observer;

    ReminderLoader(Context context, Bundle bundle) {
        super(context);
        this.bundle = bundle;
        contentValues = new ContentValues();
        TITLE_FINISHED = context.getString(R.string.finished);
        TITLE_TODAY = context.getString(R.string.today);
        TITLE_TOMORROW = context.getString(R.string.tomorrow);
        TITLE_THIS_WEEK = context.getString(R.string.thisWeek);
        TITLE_THIS_MONTH = context.getString(R.string.thisMonth);
        TITLE_OTHERS = context.getString(R.string.others);
        observer = new ForceLoadContentObserver();
    }

    @Override
    protected void onStartLoading() {
        if (cachedData != null) {
            deliverResult(cachedData);
        }
        if (takeContentChanged() || cachedData == null) {
            forceLoad();
        }
    }

    /* Runs on a worker thread */
    @Override
    public List<Entity> loadInBackground() {
        Log.d("ReminderLoader", "loader will load data");
        synchronized (this) {
            if (isLoadInBackgroundCanceled()) {
                throw new OperationCanceledException();
            }
            cancellationSignal = new CancellationSignal();
        }
        try {
            if (bundle != null) {
                if (bundle.containsKey(KEY_DELETE_ITEM_ID)) {
                    deleteItem(bundle.getLong(KEY_DELETE_ITEM_ID));
                } else if (bundle.containsKey(KEY_DELETE_ARRAY_ITEMS_ID)) {
                    deleteItems(bundle.getLongArray(KEY_DELETE_ARRAY_ITEMS_ID));
                } else if (bundle.containsKey(KEY_RESTORE_ITEM_ID)) {
                    restoreItem(bundle.getLong(KEY_RESTORE_ITEM_ID));
                } else if (bundle.containsKey(KEY_RESTORE_ARRAY_ITEMS_ID)) {
                    restoreItems(bundle.getLongArray(KEY_RESTORE_ARRAY_ITEMS_ID));
                }
            }
            return fetchAll();
        } finally {
            synchronized (this) {
                cancellationSignal = null;
            }
        }
    }

    @Override
    public void deliverResult(List<Entity> data) {
        cachedData = data;
        if (bundle != null) {
            if (bundle.containsKey(KEY_DELETE_ITEM_ID)) {
                Intent deleteIntent = new Intent(ACTION_SHOW_RESTORE_MSG);
                deleteIntent.putExtra(KEY_DELETE_ITEM_ID, bundle.getLong(KEY_DELETE_ITEM_ID));
                LocalBroadcastManager.getInstance(getContext())
                        .sendBroadcast(deleteIntent);
            } else if (bundle.containsKey(KEY_DELETE_ARRAY_ITEMS_ID)) {
                Intent deleteItemsIntent = new Intent(ACTION_SHOW_RESTORE_ITEMS_MSG);
                deleteItemsIntent.putExtra(KEY_DELETE_ARRAY_ITEMS_ID, bundle.getLongArray(KEY_DELETE_ARRAY_ITEMS_ID));
                LocalBroadcastManager.getInstance(getContext())
                        .sendBroadcast(deleteItemsIntent);
            }
        }
        super.deliverResult(data);
    }

    @Override
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            if (cancellationSignal != null) {
                cancellationSignal.cancel();
            }
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        this.bundle = null;
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    private List<Entity> fetchAll() {
        List<Entity> dataSet = new ArrayList<>();
        cursor = ContentResolverCompat.query(getContext().getContentResolver(),
                NextopicContract.ReminderEntry.buildTopicUri(),
                null,
                null,
                null,
                null,
                cancellationSignal);
        if (cursor != null) {
            try {
                cursor.registerContentObserver(observer);
                Calendar today = Calendar.getInstance();
                today.set(Calendar.HOUR_OF_DAY, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MILLISECOND, 0);

                ArrayList<Entity> finishedList = new ArrayList<>();
                ArrayList<Entity> todayList = new ArrayList<>();
                ArrayList<Entity> tomorrowList = new ArrayList<>();
                ArrayList<Entity> thisWeekList = new ArrayList<>();
                ArrayList<Entity> thisMonthList = new ArrayList<>();
                ArrayList<Entity> normalList = new ArrayList<>();
                ArrayList<Entity> otherList = new ArrayList<>();

                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    String dateString = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_DATE));
                    boolean finished = cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_FINISHED)) > 0;
                    Calendar currentDate = Utils.stringToDate(dateString);

                    Reminder reminder = Reminder.from(cursor);

                    Calendar currentTomorrow = (Calendar) today.clone();
                    currentTomorrow.add(Calendar.DAY_OF_MONTH, 1);

                    Calendar reminderTomorrow = (Calendar) currentDate.clone();
                    reminderTomorrow.add(Calendar.DAY_OF_MONTH, 1);


                    if (!finished && dateString.isEmpty()) {
                        normalList.add(reminder);
                    } else if (finished) {
                        if (finishedList.size() == 0) finishedList.add(new ReminderHeader(TITLE_FINISHED));
                        finishedList.add(reminder);
                    } else if (currentDate.equals(today)) {
                        if (todayList.size() == 0) todayList.add(new ReminderHeader(TITLE_TODAY));
                        todayList.add(reminder);
                    } else if (reminderTomorrow.equals(currentTomorrow)) {
                        if (tomorrowList.size() == 0) tomorrowList.add(new ReminderHeader(TITLE_TOMORROW));
                        tomorrowList.add(reminder);
                    } else if (currentDate.get(Calendar.WEEK_OF_MONTH) == today.get(Calendar.WEEK_OF_MONTH)) {
                        if (thisWeekList.size() == 0) thisWeekList.add(new ReminderHeader(TITLE_THIS_WEEK));
                        thisWeekList.add(reminder);
                    } else if (currentDate.get(Calendar.MONTH) == today.get(Calendar.MONTH)) {
                        if (thisMonthList.size() == 0) thisMonthList.add(new ReminderHeader(TITLE_THIS_MONTH));
                        thisMonthList.add(reminder);
                    } else {
                        if (otherList.size() == 0) otherList.add(new ReminderHeader(TITLE_OTHERS));
                        otherList.add(reminder);
                    }
                }

                dataSet.addAll(normalList);
                dataSet.addAll(todayList);
                dataSet.addAll(tomorrowList);
                dataSet.addAll(thisWeekList);
                dataSet.addAll(thisMonthList);
                dataSet.addAll(finishedList);
                dataSet.addAll(otherList);

            } catch (RuntimeException ex) {
                cursor.close();
                throw ex;
            }
        }
        return dataSet;
    }

    private void deleteItem(long id) {
        contentValues.put(COLUMN_NAME_DELETED, true);
        getContext().getContentResolver()
                .update(NextopicContract.ReminderEntry.buildTopicUriWith(id),
                        contentValues,
                        null,
                        null);
    }

    private void deleteItems(long[] indexListToDelete) {
        for (long id : indexListToDelete) {
            deleteItem(id);
        }
    }

    private void restoreItem(long id) {
        contentValues.put(COLUMN_NAME_DELETED, false);
        getContext().getContentResolver()
                .update(NextopicContract.ReminderEntry.buildTopicUriWith(id),
                        contentValues,
                        null,
                        null);
    }

    private void restoreItems(long[] itemsToRestore) {
        for (long id : itemsToRestore) {
            restoreItem(id);
        }
    }
}
