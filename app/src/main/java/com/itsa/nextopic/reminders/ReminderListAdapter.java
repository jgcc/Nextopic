package com.itsa.nextopic.reminders;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itsa.nextopic.R;
import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.entities.Reminder;
import com.itsa.nextopic.entities.ReminderHeader;
import com.itsa.nextopic.ui.MultiSelectAdapter;
import com.itsa.nextopic.ui.RecyclerDiffCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.itsa.nextopic.data.source.Entity.HEADER;
import static com.itsa.nextopic.data.source.Entity.TITLE_ALARM;
import static com.itsa.nextopic.data.source.Entity.TITLE_ALL;
import static com.itsa.nextopic.data.source.Entity.TITLE_DESCRIPTION;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public class ReminderListAdapter extends MultiSelectAdapter<BaseHolder> {

    private List<Entity> dataSet;
    private final DateFormat dateFormat;
    private final DateFormat timeFormat;

    ReminderListAdapter() {
        dataSet = new ArrayList<>();
        this.dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyy", Locale.getDefault());
        this.timeFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case HEADER:
                view = inflater.inflate(R.layout.item_list_reminder_header, parent, false);
                return new HeaderHolder(view);
            case TITLE_ALL:
                view = inflater.inflate(R.layout.item_list_reminder_type_4, parent, false);
                return new AllHolder(view);
            case TITLE_ALARM:
                view = inflater.inflate(R.layout.item_list_reminder_type_3, parent, false);
                return new TitleAlarmHolder(view);
            case TITLE_DESCRIPTION:
                view = inflater.inflate(R.layout.item_list_reminder_type_2, parent, false);
                return new TitleDescriptionHolder(view);
            default:
                view = inflater.inflate(R.layout.item_list_reminder_type_1, parent, false);
                return new TitleOnlyHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder baseHolder, int position) {
        Entity entity = dataSet.get(position);
        BaseHolder holder = (BaseHolder) baseHolder;

        if (entity instanceof Reminder) {
            Reminder reminder = (Reminder)entity;
            holder.setTitle(reminder.getTitle());
            holder.setSelected(reminder.isSelected());

            String dateTime;
            switch (reminder.getType()) {
                case TITLE_ALL:
                    holder.setNotes(reminder.getNotes());
                    dateTime = dateFormat.format(reminder.getDate().getTime()) + " a las " +
                            timeFormat.format(reminder.getTime().getTime());
                    holder.setTime(dateTime, reminder.isFinished());
                    break;
                case TITLE_ALARM:
                    dateTime = dateFormat.format(reminder.getDate().getTime()) + " a las " +
                            timeFormat.format(reminder.getTime().getTime());
                    holder.setTime(dateTime, reminder.isFinished());
                    break;
                default:
                    holder.setNotes(reminder.getNotes());
                    break;
            }

        } else {
            ReminderHeader header = (ReminderHeader) entity;
            holder.setTitle(header.getTitle());
        }

    }

    @Override
    public void itemSelected(int position, boolean selected) {
        dataSet.get(position).setSelected(selected);
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public long getItemId(int position) {
        return dataSet.get(position).getId();
    }

    @Override
    public int getItemViewType(int position) {
        return dataSet.get(position).getType();
    }

    void swap(List<Entity> data) {
        final RecyclerDiffCallback diffCallback = new RecyclerDiffCallback(dataSet, data);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        dataSet.clear();
        dataSet.addAll(data);
        diffResult.dispatchUpdatesTo(this);
    }

    private static class HeaderHolder extends BaseHolder {
        HeaderHolder(View itemView) {
            super(itemView, R.id.txtReminderDate);
        }
    }

    private static class AllHolder extends BaseHolder {
        private TextView time;
        private TextView notes;
        AllHolder(View itemView) {
            super(itemView, R.id.txtReminderTitle);
            notes = itemView.findViewById(R.id.txtReminderNotes);
            time = itemView.findViewById(R.id.txtReminderHour);
        }

        @Override
        public void setNotes(String notes) {
            this.notes.setText(notes);
        }

        @Override
        public void setTime(String time, boolean isFinished) {
            this.time.setText(time);
            if (isFinished) {
                this.time.setTextColor(redColor);
            }
        }
    }

    private static class TitleAlarmHolder extends BaseHolder {
        private TextView time;
        TitleAlarmHolder(View itemView) {
            super(itemView, R.id.txtReminderTitle);
            time = itemView.findViewById(R.id.txtReminderHour);
        }

        @Override
        public void setTime(String time, boolean isFinished) {
            this.time.setText(time);
            if (isFinished) {
                this.time.setTextColor(redColor);
            }
        }
    }

    private static class TitleDescriptionHolder extends BaseHolder {
        private TextView notes;
        TitleDescriptionHolder(View itemView) {
            super(itemView, R.id.txtReminderTitle);
            notes = itemView.findViewById(R.id.txtReminderNotes);
        }

        @Override
        public void setNotes(String notes) {
            this.notes.setText(notes);
        }
    }

    private static class TitleOnlyHolder extends BaseHolder {
        TitleOnlyHolder(View itemView) {
            super(itemView, R.id.txtReminderTitle);
        }
    }

}
