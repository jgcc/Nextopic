package com.itsa.nextopic.topics;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.ContentResolverCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.os.CancellationSignal;
import android.util.Log;

import com.itsa.nextopic.R;
import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.data.source.local.NextopicContract;
import com.itsa.nextopic.entities.Topic;
import com.itsa.nextopic.entities.TopicHeader;

import java.util.ArrayList;
import java.util.List;

import static com.itsa.nextopic.MainActivity.ACTION_SHOW_RESTORE_ITEMS_MSG;
import static com.itsa.nextopic.MainActivity.ACTION_SHOW_RESTORE_MSG;
import static com.itsa.nextopic.MainActivity.KEY_DELETE_ARRAY_ITEMS_ID;
import static com.itsa.nextopic.MainActivity.KEY_DELETE_ITEM_ID;
import static com.itsa.nextopic.MainActivity.KEY_RESTORE_ARRAY_ITEMS_ID;
import static com.itsa.nextopic.MainActivity.KEY_RESTORE_ITEM_ID;
import static com.itsa.nextopic.data.source.local.NextopicContract.SubjectEntry.COLUMN_SUBJECTS_NAME;
import static com.itsa.nextopic.data.source.local.NextopicContract.TopicEntry.COLUMN_SUBJECT_ID;
import static com.itsa.nextopic.data.source.local.NextopicContract.TopicEntry.COLUMN_TURN;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public class TopicsLoader extends AsyncTaskLoader<List<Entity>> {

    public static final String SEARCH_KEY = "SEARCH_KEY";

    private CancellationSignal mCancellationSignal;
    private List<Entity> cachedData;
    private Bundle bundle;
    private final ContentValues contentValues;
    private String filter;

    TopicsLoader(Context context, Bundle bundle) {
        super(context);
        this.bundle = bundle;
        this.contentValues = new ContentValues();
        filter = "";
    }

    @Override
    protected void onStartLoading() {
        if (cachedData == null) {
            forceLoad();
        } else {
            deliverResult(cachedData);
        }
    }


    @Override
    public List<Entity> loadInBackground() {
        Log.d("TopicsLoader", "loader will load data");
        synchronized (this) {
            if (isLoadInBackgroundCanceled()) {
                throw new android.support.v4.os.OperationCanceledException();
            }
            mCancellationSignal = new CancellationSignal();
        }
        try {
            if (bundle != null) {
                if (bundle.containsKey(KEY_DELETE_ITEM_ID)) {
                    deleteItem(bundle.getLong(KEY_DELETE_ITEM_ID));
                } else if (bundle.containsKey(KEY_DELETE_ARRAY_ITEMS_ID)) {
                    deleteItems(bundle.getLongArray(KEY_DELETE_ARRAY_ITEMS_ID));
                } else if (bundle.containsKey(KEY_RESTORE_ITEM_ID)) {
                    restoreItem(bundle.getLong(KEY_RESTORE_ITEM_ID));
                } else if (bundle.containsKey(KEY_RESTORE_ARRAY_ITEMS_ID)) {
                    restoreItems(bundle.getLongArray(KEY_RESTORE_ARRAY_ITEMS_ID));
                } else if (bundle.containsKey(SEARCH_KEY)) {
                    filter = bundle.getString(SEARCH_KEY, "");
                }

            }

            Cursor cursor;
            if (filter.isEmpty()) {
                cursor = ContentResolverCompat.query(getContext().getContentResolver(),
                        NextopicContract.TopicEntry.buildTopicUri(),
                        null,
                        null,
                        null,
                        null,
                        mCancellationSignal);
            } else {
                cursor = ContentResolverCompat.query(getContext().getContentResolver(),
                        NextopicContract.TopicEntry.buildTopicUri(),
                        null,
                        NextopicContract.TopicEntry.COLUMN_TOPIC_NAME + " like ?",
                        new String[]{"%" + filter +"%"},
                        null,
                        mCancellationSignal);
            }
            return fetchAll(cursor);
        } finally {
            synchronized (this) {
                mCancellationSignal = null;
            }
        }
    }

    @Override
    public void deliverResult(List<Entity> data) {
        cachedData = data;
        if (bundle != null) {
            if (bundle.containsKey(KEY_DELETE_ITEM_ID)) {
                Intent deleteIntent = new Intent(ACTION_SHOW_RESTORE_MSG);
                deleteIntent.putExtra(KEY_DELETE_ITEM_ID, bundle.getLong(KEY_DELETE_ITEM_ID));
                LocalBroadcastManager.getInstance(getContext())
                        .sendBroadcast(deleteIntent);
            } else if (bundle.containsKey(KEY_DELETE_ARRAY_ITEMS_ID)) {
                Intent deleteItemsIntent = new Intent(ACTION_SHOW_RESTORE_ITEMS_MSG);
                deleteItemsIntent.putExtra(KEY_DELETE_ARRAY_ITEMS_ID, bundle.getLongArray(KEY_DELETE_ARRAY_ITEMS_ID));
                LocalBroadcastManager.getInstance(getContext())
                        .sendBroadcast(deleteItemsIntent);
            }
        }
        super.deliverResult(data);
    }

    @Override
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            if (mCancellationSignal != null) {
                mCancellationSignal.cancel();
            }
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        this.bundle = null;
    }

    private List<Entity> fetchAll(Cursor cursor) {
        List<Entity> dataSet = new ArrayList<>();
        if (cursor != null) {
            try {
                long prevId = 0;
                int prevTurn = -1;
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    long subjectID = cursor.getLong(cursor.getColumnIndex(COLUMN_SUBJECT_ID));
                    String subjectName = cursor.getString(cursor.getColumnIndex(COLUMN_SUBJECTS_NAME));
                    int turn = cursor.getInt(cursor.getColumnIndex(COLUMN_TURN));
                    String sTurn = turn > 0 ? getContext().getString(R.string.evening) : getContext().getString(R.string.morning);
                    if (i == 0) {
                        dataSet.add(new TopicHeader(subjectName, sTurn));
                    } else if (prevId != subjectID || prevTurn != turn){
                        dataSet.add(new TopicHeader(subjectName, sTurn));
                    }
                    dataSet.add(Topic.from(cursor));
                    prevId = subjectID;
                    prevTurn = turn;
                }
                cursor.close();
            } catch (RuntimeException e){
                cursor.close();
                throw e;
            }
        }
        return dataSet;
    }

    private void deleteItem(long id) {
        contentValues.put(NextopicContract.TopicEntry.COLUMN_DELETED, true);
        getContext().getContentResolver()
                .update(NextopicContract.TopicEntry.buildTopicUriWith(id), contentValues, null, null);

    }

    private void deleteItems(long[] idItemsToDelete) {
        for(long id : idItemsToDelete) {
            deleteItem(id);
        }
    }
    private void restoreItem(long id) {
        contentValues.put(NextopicContract.TopicEntry.COLUMN_DELETED, false);
        getContext().getContentResolver()
                .update(NextopicContract.TopicEntry.buildTopicUriWith(id), contentValues, null, null);

    }

    private void restoreItems(long[] idItemsToRestore) {
        for (long id : idItemsToRestore) {
            restoreItem(id);
        }
    }

}
