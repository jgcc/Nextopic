package com.itsa.nextopic.topics;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManagerFix;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.itsa.nextopic.R;
import com.itsa.nextopic.data.source.local.NextopicContract;
import com.itsa.nextopic.entities.Topic;
import com.itsa.nextopic.ui.CustomDatePicker;
import com.itsa.nextopic.ui.SaveDialog;

import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import static android.provider.BaseColumns._ID;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public class SaveTopic_Activity extends AppCompatActivity implements Observer {

    private Spinner subjectSpinner;
    private EditText editTextTheme;
    private RadioGroup radioGroupTurn;
    private CustomDatePicker scheduleDatePicker;
    private CustomDatePicker realDatePicker;
    private SimpleCursorAdapter adapter;
    private Cursor cursor;

    private final String []from = {NextopicContract.SubjectEntry.COLUMN_SUBJECTS_NAME};
    private final int []to = {android.R.id.text1};

    private Topic topic;
    private boolean hasChanged;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_topic);
        setTitle(R.string.add_topic);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        hasChanged = false;

        subjectSpinner = findViewById(R.id.editTextSubject);
        editTextTheme = findViewById(R.id.inputTheme);
        radioGroupTurn = findViewById(R.id.radioGroupTurn);
        scheduleDatePicker = findViewById(R.id.Schedule);
        realDatePicker = findViewById(R.id.realDate);

        scheduleDatePicker.setDate(Calendar.getInstance());

        SharedPreferences preferences = PreferenceManagerFix.getDefaultSharedPreferences(this);
        String semesterId = preferences.getString(getString(R.string.semester), "1");
        String careerId = preferences.getString(getString(R.string.career), "1");

        cursor = getContentResolver()
                .query(NextopicContract.SubjectEntry.buildTopicUri(),
                        null,
                        NextopicContract.SubjectEntry.COLUMN_SUBJECTS_CAREER_ID + "=? and " + NextopicContract.SubjectEntry.COLUMN_SUBJECTS_SEMESTER_ID + "=?",
                        new String[]{careerId, semesterId},
                        null);
        adapter = new SimpleCursorAdapter(this, android.R.layout.simple_spinner_item, cursor, from, to, 1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subjectSpinner.setAdapter(adapter);

        editTextTheme.addTextChangedListener(new TextWatcher() {
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
                topic.setName(s.toString());
            }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override public void afterTextChanged(Editable s) {}
        });
        radioGroupTurn.setOnCheckedChangeListener((group, checkedId) -> {
            int turn = checkedId == R.id.radioMorning ? 0 : 1;
            topic.setTurn(turn);
        });
        subjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                topic.setSubjectId(id);
            }
            @Override public void onNothingSelected(AdapterView<?> parent) {}
        });
        scheduleDatePicker.onDateSetListener((view, year, month, dayOfMonth) ->
            topic.setScheduleDate(scheduleDatePicker.getDate())
        );
        realDatePicker.onDateSetListener((view, year, month, dayOfMonth) ->
            topic.setRealDate(realDatePicker.getDate())
        );

        if (getIntent().getExtras() != null) {
            populateData();
        } else {
            int turn = radioGroupTurn.getCheckedRadioButtonId() == R.id.radioMorning ? 0 : 1;
            topic = new Topic(adapter.getItemId(0), scheduleDatePicker.getDate(), turn);
        }
        topic.addObserver(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (cursor != null) {
            cursor.close();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_topic, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (hasChanged) {
                    SaveDialog.newInstance(R.string.warning_dialog_msg)
                            .show(getSupportFragmentManager(), "MTAG");
                } else {
                    finish();
                }
                return true;
            case R.id.action_save:
                save();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void save() {
        String theme = editTextTheme.getText().toString();

        if (theme.isEmpty()) {
            editTextTheme.setError("tema no puede ser vacio");
            return;
        }

        if (getIntent().getExtras() == null) {
            getContentResolver().insert(NextopicContract.TopicEntry.buildTopicUri(), topic.getContentValues());
        } else {
            getContentResolver().update(NextopicContract.TopicEntry.buildTopicUriWith(topic.getId()), topic.getContentValues(), null, null);
        }

        Intent data = new Intent();
        setResult(RESULT_OK, data);
        finish();
    }

    private void populateData() {
        long id = getIntent().getLongExtra(_ID, -1);
        String selection = NextopicContract.TopicEntry.TABLE_NAME+"."+_ID + " = ?";
        String []args = {String.valueOf(id)};
        Cursor cursor = getContentResolver().query(NextopicContract.TopicEntry.buildTopicUri(), null, selection, args, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                topic = Topic.from(cursor);
                editTextTheme.setText(topic.getTheme());
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (topic.getSubjectId() == adapter.getItemId(i)) {
                        subjectSpinner.setSelection(i);
                    }
                }
                scheduleDatePicker.setDate(topic.getScheduleDate());
                realDatePicker.setDate(topic.getRealDate());
                int checkedId = topic.getTurn() > 0 ? R.id.radioEvening : R.id.radioMorning;
                radioGroupTurn.check(checkedId);
            }
            cursor.close();
        }
    }

    // TODO: Check when all fields are empty
    @Override
    public void update(Observable o, Object arg) {
        hasChanged = true;
    }
}
