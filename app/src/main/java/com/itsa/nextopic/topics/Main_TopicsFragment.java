package com.itsa.nextopic.topics;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.itsa.nextopic.MainActivity;
import com.itsa.nextopic.R;
import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.ui.RecyclerTouchListener;
import com.itsa.nextopic.ui.SwipeMenu;

import java.util.Collections;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.provider.BaseColumns._ID;
import static com.itsa.nextopic.MainActivity.ACTION_MODE_INSTANCE_STATE;
import static com.itsa.nextopic.MainActivity.ACTION_SHOW_RESTORE_ITEMS_MSG;
import static com.itsa.nextopic.MainActivity.ACTION_SHOW_RESTORE_MSG;
import static com.itsa.nextopic.MainActivity.KEY_DELETE_ARRAY_ITEMS_ID;
import static com.itsa.nextopic.MainActivity.KEY_DELETE_ITEM_ID;
import static com.itsa.nextopic.MainActivity.KEY_RESTORE_ARRAY_ITEMS_ID;
import static com.itsa.nextopic.MainActivity.KEY_RESTORE_ITEM_ID;
import static com.itsa.nextopic.MainActivity.REQUEST_UPDATE_ITEM;
import static com.itsa.nextopic.MainActivity.STATE_RECYCLERVIEW;

/**
 * ${PACKAGE_NAME}
 * Created by jose carabez on 19/03/2017.
 * Nextopic
 */

public class Main_TopicsFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<List<Entity>>,
        ActionMode.Callback,
        SearchView.OnQueryTextListener {

    public final static int LOADER_ID = 33456;

    private final IntentFilter restoreFilter = new IntentFilter(ACTION_SHOW_RESTORE_MSG);
    private final IntentFilter restoreItemsFilter = new IntentFilter(ACTION_SHOW_RESTORE_ITEMS_MSG);

    private MainActivity mainActivity;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private TopicsListAdapter adapter;
    private TextView emptyMessage;

    private ActionMode actionMode;
    private int statusBarColor;
    private boolean isActionMode;
    private String selectedText;

    private BroadcastReceiver myBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null) {
                if (intent.getAction().equals(ACTION_SHOW_RESTORE_MSG)) {
                    showRestoreMessage(intent.getLongExtra(KEY_DELETE_ITEM_ID, -1));
                } else if (intent.getAction().equals(ACTION_SHOW_RESTORE_ITEMS_MSG)) {
                    showRestoreMultipleItems(intent.getLongArrayExtra(KEY_DELETE_ARRAY_ITEMS_ID));
                }
            }
        }
    };

    public static Main_TopicsFragment newInstance(){
        return new Main_TopicsFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            mainActivity = (MainActivity) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View view = getView() != null ? getView() :
                inflater.inflate(R.layout.main_list_topics, container, false);

        selectedText = getString(R.string.selected);

        emptyMessage = view.findViewById(R.id.empty_msg);
        layoutManager = new LinearLayoutManager(mainActivity);
        adapter = new TopicsListAdapter();

        recyclerView = view.findViewById(R.id.topicList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (adapter.isMultiSelectMode()) {
                    actionMode.setTitle(selectedText + " " + adapter.getSelectionCount());
                    if (adapter.getSelectionCount() == 0) {
                        actionMode.finish();
                    }
                } else {
                    Intent intent = new Intent(getContext(), SaveTopic_Activity.class);
                    intent.putExtra(_ID, adapter.getItemId(position));
                    startActivityForResult(intent, REQUEST_UPDATE_ITEM);
                }
            }
            @Override public void onLongClick(View view, int position) {
                if (adapter.isMultiSelectMode()) {
                    mainActivity.startSupportActionMode(Main_TopicsFragment.this);
                }
            }
        }));

        if (getContext() != null) {
            ItemTouchHelper touchHelper = new ItemTouchHelper(new SwipeMenu(getContext(), position -> {
                Bundle bundle = new Bundle();
                bundle.putLong(KEY_DELETE_ITEM_ID, adapter.getItemId(position));
                getLoaderManager().restartLoader(LOADER_ID, bundle, Main_TopicsFragment.this);
            }));
            touchHelper.attachToRecyclerView(recyclerView);
        }

        if (savedInstanceState != null) {
            isActionMode = savedInstanceState.getBoolean(ACTION_MODE_INSTANCE_STATE);
            adapter.onRestoreInstanceState(savedInstanceState);
            layoutManager.onRestoreInstanceState(savedInstanceState.getParcelable(STATE_RECYCLERVIEW));
        }

        if (isActionMode) {
            mainActivity.startSupportActionMode(Main_TopicsFragment.this);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( getContext() != null) {
            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getContext());
            manager.registerReceiver(myBroadcastReceiver, restoreFilter);
            manager.registerReceiver(myBroadcastReceiver, restoreItemsFilter);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getContext() != null) {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(myBroadcastReceiver);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.clearSelection();
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            getLoaderManager().restartLoader(LOADER_ID, null, this);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        adapter.onSaveInstanceState(outState);
        outState.putBoolean(ACTION_MODE_INSTANCE_STATE, isActionMode);
        outState.putParcelable(STATE_RECYCLERVIEW, layoutManager.onSaveInstanceState());
    }

    @Override
    public Loader<List<Entity>> onCreateLoader(int id, Bundle args) {
        return new TopicsLoader(getContext(), args);
    }

    @Override
    public void onLoadFinished(Loader<List<Entity>> loader, List<Entity> data) {
        if (data.size() > 0) {
            emptyMessage.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.swap(data);
        } else {
            emptyMessage.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Entity>> loader) {
        adapter.swap(Collections.emptyList());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        //searchItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);
        searchItem.setActionView(searchView);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            statusBarColor = mainActivity.getWindow().getStatusBarColor();
            mainActivity.getWindow().setStatusBarColor(ContextCompat.getColor(mainActivity, R.color.primaryDark_selected_state));
        }
        mode.getMenuInflater().inflate(R.menu.menu_action_list, menu);
        mode.setTitle(getString(R.string.selected) + " " + adapter.getSelectionCount());
        actionMode = mode;
        isActionMode = true;
        mainActivity.fab.hide();
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                Bundle bundle = new Bundle();
                long[] itemsToDelete = Stream.of(adapter.getSelectedItems())
                        .mapToLong( i -> adapter.getItemId(i))
                        .toArray();
                bundle.putLongArray(KEY_DELETE_ARRAY_ITEMS_ID, itemsToDelete);
                mode.finish();
                getLoaderManager().restartLoader(LOADER_ID, bundle, Main_TopicsFragment.this);
                return true;
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        //return to old color of status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mainActivity.getWindow().setStatusBarColor(statusBarColor);
        }
        adapter.clearSelection();
        actionMode = null;
        isActionMode = false;
        mainActivity.fab.show();
    }

    @Override public boolean onPrepareActionMode(ActionMode mode, Menu menu) { return false; }

    public void showRestoreMessage(long id) {
        Snackbar.make(mainActivity.findViewById(R.id.fab), R.string.msg_delete_topic, Snackbar.LENGTH_LONG)
            .setAction(R.string.undo, v -> {
                Bundle bundle = new Bundle();
                bundle.putLong(KEY_RESTORE_ITEM_ID, id);
                getLoaderManager().restartLoader(LOADER_ID, bundle, Main_TopicsFragment.this);
            })
            .show();
    }

    public void showRestoreMultipleItems(long[] itemsToRestore) {
        int count = itemsToRestore.length;
        String msg = getResources().getQuantityString(R.plurals.items_to_restore, count, count);
        Snackbar.make(mainActivity.findViewById(R.id.fab), msg, Snackbar.LENGTH_LONG)
                .setAction(R.string.undo, v -> {
                    Bundle bundle = new Bundle();
                    bundle.putLongArray(KEY_RESTORE_ARRAY_ITEMS_ID, itemsToRestore);
                    getLoaderManager().restartLoader(LOADER_ID, bundle, Main_TopicsFragment.this);
                })
                .show();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Bundle bundle = new Bundle();
        bundle.putString(TopicsLoader.SEARCH_KEY, newText);
        getLoaderManager().restartLoader(LOADER_ID, bundle, Main_TopicsFragment.this);
        return true;
    }
}
