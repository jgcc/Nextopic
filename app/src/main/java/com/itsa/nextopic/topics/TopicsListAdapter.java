package com.itsa.nextopic.topics;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itsa.nextopic.R;
import com.itsa.nextopic.data.source.Entity;
import com.itsa.nextopic.entities.Topic;
import com.itsa.nextopic.entities.TopicHeader;
import com.itsa.nextopic.ui.MultiSelectAdapter;
import com.itsa.nextopic.ui.RecyclerDiffCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.itsa.nextopic.data.source.Entity.HEADER;
import static com.itsa.nextopic.data.source.Entity.THEME_TYPE_ERROR;
import static com.itsa.nextopic.data.source.Entity.THEME_TYPE_OK;


/**
 * ${PACKAGE_NAME}
 * Created by jose carabez on 02/04/2017.
 * Nextopic
 */

public class TopicsListAdapter extends MultiSelectAdapter<CustomViewHolder> {

    private List<Entity> dataSet;
    private final DateFormat dateFormat;

    TopicsListAdapter() {
        this.dataSet = new ArrayList<>();
        dateFormat = new SimpleDateFormat("EEE dd/MMMM/yyy", Locale.getDefault());

    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case HEADER:
                view = inflater.inflate(R.layout.item_list_topic_header, parent, false);
                return new TopicHeaderViewHolder(view);
            case THEME_TYPE_OK:
                view = inflater.inflate(R.layout.item_list_topic_type_2, parent, false);
                return new TopicOKViewHolder(view);
            case THEME_TYPE_ERROR:
                view = inflater.inflate(R.layout.item_list_topic_type_3, parent, false);
                return new TopicErrorViewHolder(view);
            default:
                view = inflater.inflate(R.layout.item_list_topic_type_1, parent, false);
                return new TopicNormalViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderParent, int position) {
        Entity entity = dataSet.get(position);
        CustomViewHolder holder = (CustomViewHolder) holderParent;

        if (entity instanceof Topic) {
            Topic topic = (Topic) entity;
            holder.setTheme(topic.getTheme());
            holder.setScheduleDate(dateFormat.format(topic.getScheduleDate().getTime()));
            holder.setSelected(topic.isSelected());
            switch (topic.getType()) {
                case THEME_TYPE_OK:
                    holder.setRealDate(dateFormat.format(topic.getRealDate().getTime()));
                    break;
                case THEME_TYPE_ERROR:
                    holder.setRealDate(dateFormat.format(topic.getRealDate().getTime()));
                    long dif = Math.abs(topic.getScheduleDate().getTimeInMillis() - topic.getRealDate().getTimeInMillis());
                    long days = TimeUnit.DAYS.convert(dif, TimeUnit.MILLISECONDS);
                    holder.setDelayedDays((int) days);
                    break;
            }
        } else {
            TopicHeader header = (TopicHeader) entity;
            holder.setSubject(header.getSubject());
            holder.setTurn(header.getTurn());
        }
    }

    @Override
    public void itemSelected(int position, boolean selected) {
        dataSet.get(position).setSelected(selected);
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public long getItemId(int position) {
        return dataSet.get(position).getId();
    }

    @Override
    public int getItemViewType(int position) {
        return dataSet.get(position).getType();
    }

    void swap(List<Entity> data) {
        final RecyclerDiffCallback diffCallback = new RecyclerDiffCallback(dataSet, data);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        dataSet.clear();
        dataSet.addAll(data);
        diffResult.dispatchUpdatesTo(this);
    }

    private static class TopicHeaderViewHolder extends CustomViewHolder {
        TextView subject;
        TextView turn;
        TopicHeaderViewHolder(View itemView) {
            super(itemView);
            subject = itemView.findViewById(R.id.txtSubject);
            turn = itemView.findViewById(R.id.txtTurn);
        }

        @Override
        public void setSubject(String subject) {
            this.subject.setText(subject);
        }

        @Override
        public void setTurn(String turn) {
            this.turn.setText(turn);
        }
    }

    private static class TopicNormalViewHolder extends CustomViewHolder {
        TextView theme;
        TextView scheduleDate;
        TopicNormalViewHolder(View itemView) {
            super(itemView);
            theme = itemView.findViewById(R.id.txtTheme);
            scheduleDate = itemView.findViewById(R.id.txtScheduleDate);
        }

        @Override
        public void setTheme(String theme) {
            this.theme.setText(theme);
        }

        @Override
        public void setScheduleDate(String date) {
            this.scheduleDate.setText(date);
        }
    }

    private static class TopicOKViewHolder extends CustomViewHolder {
        public TextView theme;
        TextView scheduleDate;
        TextView realDate;
        TopicOKViewHolder(View itemView) {
            super(itemView);
            theme = itemView.findViewById(R.id.txtTheme);
            scheduleDate = itemView.findViewById(R.id.txtScheduleDate);
            realDate = itemView.findViewById(R.id.txtRealDate);
        }
        @Override
        public void setTheme(String theme) {
            this.theme.setText(theme);
        }

        @Override
        public void setScheduleDate(String date) {
            this.scheduleDate.setText(date);
        }

        @Override
        public void setRealDate(String date) {
            this.realDate.setText(date);
        }
    }

    private static class TopicErrorViewHolder extends CustomViewHolder {
        public TextView theme;
        TextView scheduleDate;
        TextView realDate;
        TextView delayedDays;
        TopicErrorViewHolder(View itemView) {
            super(itemView);
            theme = itemView.findViewById(R.id.txtTheme);
            scheduleDate = itemView.findViewById(R.id.txtScheduleDate);
            realDate = itemView.findViewById(R.id.txtRealDate);
            delayedDays = itemView.findViewById(R.id.txtDays);
        }

        @Override
        public void setTheme(String theme) {
            this.theme.setText(theme);
        }

        @Override
        public void setScheduleDate(String date) {
            this.scheduleDate.setText(date);
        }

        @Override
        public void setRealDate(String date) {
            this.realDate.setText(date);
        }

        @Override
        public void setDelayedDays(int days) {
            delayedDays.setText(String.valueOf(days));
        }
    }
}
