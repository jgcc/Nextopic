package com.itsa.nextopic.topics;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.itsa.nextopic.R;

/**
 * Created by jose carabez on 09/11/17.
 */

public abstract class CustomViewHolder extends RecyclerView.ViewHolder implements TopicViewRow {

    View itemView;
    int selectedColor;
    Drawable normal;

    public CustomViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        selectedColor = ContextCompat.getColor(itemView.getContext(), R.color.selected_color);
        normal = itemView.getBackground();
    }

    @Override
    public void setSubject(String subject) {

    }

    @Override
    public void setTheme(String theme) {

    }

    @Override
    public void setTurn(String turn) {

    }

    @Override
    public void setScheduleDate(String date) {

    }

    @Override
    public void setRealDate(String date) {

    }

    @Override
    public void setDelayedDays(int days) {

    }

    @Override
    public void setSelected(boolean selected) {
        if (selected) {
            itemView.setBackgroundColor(selectedColor);
        } else {
            itemView.setBackgroundDrawable(normal);
        }
    }
}
