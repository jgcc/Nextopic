package com.itsa.nextopic;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * ${PACKAGE_NAME}
 * Created by jose carabez on 29/03/2017.
 * Nextopic
 */

public final class Utils {

    private static final DateFormat dateSqliteFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static final DateFormat dateUserFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

    private static final DateFormat time24Format = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private static final DateFormat time12Format = new SimpleDateFormat("hh:mm a", Locale.getDefault());

    private Utils() {
    }

    public static Calendar stringToDate(String date) {
        Calendar calendar = Calendar.getInstance();
        try {
            if (!date.isEmpty()){
                if (date.contains("/")) {
                    calendar.setTime(dateUserFormat.parse(date));
                } else {
                    calendar.setTime(dateSqliteFormat.parse(date));
                }
            }

        } catch (ParseException e) {
            Log.e("Utils", "stringToDate", e);
        }
        return calendar;
    }

    public static String dateToString(Calendar date) {
        return dateSqliteFormat.format(date.getTime());
    }

    public static Calendar stringToTime(String time) {
        Calendar calendar = Calendar.getInstance();
        if (!time.isEmpty()) {
            try {
                if (time.contains(" ")) {
                    calendar.setTime(time12Format.parse(time));
                } else {
                    calendar.setTime(time24Format.parse(time));
                }
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("Utils", "stringToTime", e);
            }
        }
        return calendar;
    }

    public static String timeToString(Calendar time) {
        return time24Format.format(time.getTime());
    }

}
