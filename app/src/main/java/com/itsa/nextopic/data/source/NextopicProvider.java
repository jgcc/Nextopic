package com.itsa.nextopic.data.source;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.itsa.nextopic.data.source.local.DatabaseHelper;
import com.itsa.nextopic.data.source.local.NextopicContract;

import static android.provider.BaseColumns._ID;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public class NextopicProvider extends ContentProvider {

    private static final int TOPICS = 100;
    private static final int TOPIC_ID = 101;
    private static final int SUBJECTS = 200;
    private static final int SUBJECT_ID = 201;
    private static final int REMINDERS = 300;
    private static final int REMINDER_ID = 301;
    private static final int SCHEDULES = 400;
    private static final int SCHEDULE_ID = 401;

    private static final UriMatcher uriMatcher = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = NextopicContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, NextopicContract.TopicEntry.TABLE_NAME, TOPICS);
        matcher.addURI(authority, NextopicContract.TopicEntry.TABLE_NAME + "/#", TOPIC_ID);
        matcher.addURI(authority, NextopicContract.SubjectEntry.TABLE_NAME, SUBJECTS);
        matcher.addURI(authority, NextopicContract.SubjectEntry.TABLE_NAME + "/#", SUBJECT_ID);
        matcher.addURI(authority, NextopicContract.ReminderEntry.TABLE_NAME, REMINDERS);
        matcher.addURI(authority, NextopicContract.ReminderEntry.TABLE_NAME + "/#", REMINDER_ID);
        matcher.addURI(authority, NextopicContract.ScheduleEntry.TABLE_NAME, SCHEDULES);
        matcher.addURI(authority, NextopicContract.ScheduleEntry.TABLE_NAME + "/#", SCHEDULE_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor cursor;
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        switch (uriMatcher.match(uri)) {
            case TOPICS:
                queryBuilder.setTables(
                    NextopicContract.TopicEntry.TABLE_NAME +
                    " INNER JOIN " +
                    NextopicContract.SubjectEntry.TABLE_NAME +
                    " ON " +
                    NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_SUBJECT_ID +
                    " = " +
                    NextopicContract.SubjectEntry.TABLE_NAME + "." + _ID +
                    " AND " +
                    NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_DELETED +
                    " = 0");
                projection = new String[] {
                        NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry._ID,
                        NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_TOPIC_NAME,
                        NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_SUBJECT_ID,
                        NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_SCHEDULE_DATE,
                        NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_REAL_DATE,
                        NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_TURN,
                        NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_DELETED,
                        NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_UUID,
                        NextopicContract.SubjectEntry.TABLE_NAME + "." + NextopicContract.SubjectEntry.COLUMN_SUBJECTS_NAME
                };
                sortOrder = NextopicContract.SubjectEntry.TABLE_NAME + "." + NextopicContract.SubjectEntry.COLUMN_SUBJECTS_NAME + "," +
                        NextopicContract.TopicEntry.TABLE_NAME + "." + NextopicContract.TopicEntry.COLUMN_TURN;
                break;
            case TOPIC_ID:
                queryBuilder.setTables(NextopicContract.TopicEntry.TABLE_NAME);
                queryBuilder.appendWhere(_ID + " = " + uri.getLastPathSegment());
                break;
            case SUBJECTS:
                // TODO query with semester and carrer
                queryBuilder.setTables(NextopicContract.SubjectEntry.TABLE_NAME);
                break;
            case REMINDERS:
                queryBuilder.setTables(NextopicContract.ReminderEntry.TABLE_NAME);
                queryBuilder.appendWhere(NextopicContract.ReminderEntry.COLUMN_NAME_DELETED +" = 0");
                if (sortOrder == null) {
                    sortOrder = NextopicContract.ReminderEntry.COLUMN_NAME_FINISHED + " ASC," +
                            "DATE(" + NextopicContract.ReminderEntry.TABLE_NAME + "." + NextopicContract.ReminderEntry.COLUMN_NAME_DATE + "), "+
                            NextopicContract.ReminderEntry.COLUMN_NAME_TITLE;
                }
                break;
            case REMINDER_ID:
                queryBuilder.setTables(NextopicContract.ReminderEntry.TABLE_NAME);
                queryBuilder.appendWhere(NextopicContract.ReminderEntry.COLUMN_NAME_DELETED +" = 0 and "+
                    _ID + " = "+ uri.getLastPathSegment());
                break;
            case SCHEDULES:
                queryBuilder.setTables(
                        NextopicContract.ScheduleEntry.TABLE_NAME +
                                " INNER JOIN " +
                                NextopicContract.SubjectEntry.TABLE_NAME +
                                " ON " +
                                NextopicContract.ScheduleEntry.TABLE_NAME + "." + NextopicContract.ScheduleEntry.COLUMN_ID_SUBJECT +
                                " = " +
                                NextopicContract.SubjectEntry.TABLE_NAME + "." + _ID +
                                " AND " +
                                NextopicContract.ScheduleEntry.TABLE_NAME + "." + NextopicContract.ScheduleEntry.COLUMN_DELETED +
                                " = 0 " +
                                " AND " +
                                selection);
                projection = new String[] {
                        NextopicContract.ScheduleEntry.TABLE_NAME + "." + NextopicContract.ScheduleEntry._ID,
                        NextopicContract.ScheduleEntry.TABLE_NAME + "." + NextopicContract.ScheduleEntry.COLUMN_ID_SUBJECT,
                        NextopicContract.ScheduleEntry.TABLE_NAME + "." + NextopicContract.ScheduleEntry.COLUMN_DAY,
                        NextopicContract.ScheduleEntry.TABLE_NAME + "." + NextopicContract.ScheduleEntry.COLUMN_TIME_START,
                        NextopicContract.ScheduleEntry.TABLE_NAME + "." + NextopicContract.ScheduleEntry.COLUMN_TIME_FINISH,
                        NextopicContract.ScheduleEntry.TABLE_NAME + "." + NextopicContract.ScheduleEntry.COLUMN_UUID,
                        NextopicContract.SubjectEntry.TABLE_NAME + "." + NextopicContract.SubjectEntry.COLUMN_SUBJECTS_NAME,
                        NextopicContract.SubjectEntry.TABLE_NAME + "." + NextopicContract.SubjectEntry.COLUMN_COLOR
                };
                sortOrder = "DATE(" + NextopicContract.ScheduleEntry.TABLE_NAME +"."+ NextopicContract.ScheduleEntry.COLUMN_TIME_START + ")";
                break;
            case SCHEDULE_ID:
                queryBuilder.setTables(NextopicContract.ScheduleEntry.TABLE_NAME);
                queryBuilder.appendWhere(NextopicContract.ScheduleEntry.COLUMN_DELETED + " = 0 and " +
                _ID + " = " + uri.getLastPathSegment());
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        cursor = queryBuilder.query(DatabaseHelper.getInstance(getContext()).getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
        if (getContext() != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case TOPICS:
                return NextopicContract.TopicEntry.CONTENT_TYPE;
            case TOPIC_ID:
                return NextopicContract.TopicEntry.CONTENT_ITEM_TYPE;
            case SUBJECTS:
                return NextopicContract.SubjectEntry.CONTENT_TYPE;
            case REMINDERS:
                return NextopicContract.ReminderEntry.CONTENT_TYPE;
            case REMINDER_ID:
                return NextopicContract.ReminderEntry.CONTENT_ITEM_TYPE;
            case SCHEDULES:
                return NextopicContract.ScheduleEntry.CONTENT_TYPE;
            case SCHEDULE_ID:
                return NextopicContract.ScheduleEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final SQLiteDatabase db = DatabaseHelper.getInstance(getContext()).getWritableDatabase();
        Uri returnUri;
        long id;
        switch (uriMatcher.match(uri)) {
            case TOPICS:
                id = db.insert(NextopicContract.TopicEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = NextopicContract.TopicEntry.buildTopicUriWith(id);
                } else {
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            case SUBJECTS:
                id = db.insert(NextopicContract.SubjectEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = NextopicContract.SubjectEntry.buildTopicUriWith(id);
                } else {
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            case REMINDERS:
                id = db.insert(NextopicContract.ReminderEntry.TABLE_NAME, null, values);
                if (id > 0) {
                    returnUri = NextopicContract.ReminderEntry.buildTopicUriWith(id);
                } else {
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            case SCHEDULES:
                //id = db.insert(NextopicContract.ScheduleEntry.TABLE_NAME, null, values);
                id = insertOrUpdate(db, uri, NextopicContract.ScheduleEntry.TABLE_NAME, values, NextopicContract.ScheduleEntry.COLUMN_UUID);
                if (id > 0) {
                    returnUri = NextopicContract.ScheduleEntry.buildUriWith(id);
                } else {
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = DatabaseHelper.getInstance(getContext()).getWritableDatabase();
        int rowDeleted;
        switch (uriMatcher.match(uri)) {
            case TOPICS:
                rowDeleted = db.delete(NextopicContract.TopicEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case REMINDERS:
                rowDeleted = db.delete(NextopicContract.ReminderEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SCHEDULES:
                rowDeleted = db.delete(NextopicContract.ScheduleEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (selection == null || rowDeleted != 0) {
            Log.d(NextopicProvider.class.getSimpleName(),"Number of rows deleted: " + rowDeleted);
            if (getContext() != null) {
                getContext().getContentResolver().notifyChange(uri, null);
            }
        }
        return rowDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = DatabaseHelper.getInstance(getContext()).getWritableDatabase();
        int roweUpdated;
        long id = ContentUris.parseId(uri);
        String where = BaseColumns._ID + " = ?";
        String[] args = {id+""};
        switch (uriMatcher.match(uri)) {
            case TOPICS:
                roweUpdated = db.update(NextopicContract.TopicEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case TOPIC_ID:
                roweUpdated = db.update(NextopicContract.TopicEntry.TABLE_NAME, values, where, args);
                break;
            case REMINDER_ID:
                roweUpdated = db.update(NextopicContract.ReminderEntry.TABLE_NAME, values, where, args);
                break;
            case SCHEDULE_ID:
                roweUpdated = db.update(NextopicContract.ScheduleEntry.TABLE_NAME, values, where, args);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (roweUpdated != 0) {
            if (getContext() != null) {
                getContext().getContentResolver().notifyChange(uri, null);
            }
        }
        return roweUpdated;
    }

    private long insertOrUpdate(SQLiteDatabase database, Uri uri, String table, ContentValues values, String column) throws SQLException {
        long id;
        try {
            id = database.insertOrThrow(table, null, values);
            Log.d(NextopicProvider.class.getName(), "values inserted: " + values.toString());
        } catch (SQLiteConstraintException e) {
            id = update(ContentUris.withAppendedId(uri, values.getAsLong(BaseColumns._ID)), values, column + "=?", new String[]{values.getAsString(column)});
            if (id == 0)
                throw  e;
            Log.d(NextopicProvider.class.getName(), "values updated: " + values.toString());
        }
        return id;
    }
}
