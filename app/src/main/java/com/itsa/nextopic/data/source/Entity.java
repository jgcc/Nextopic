package com.itsa.nextopic.data.source;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.ContentValues;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public interface Entity {

    int HEADER = 100;
    int THEME_TYPE_NORMAL = 200;
    int THEME_TYPE_OK = 300;
    int THEME_TYPE_ERROR = 400;
    int TITLE_ONLY = 500;
    int TITLE_ALARM = 600;
    int TITLE_DESCRIPTION = 700;
    int TITLE_ALL = 800;
    boolean selected = false;

    long getId();
    boolean isSelected();
    int getType();
    ContentValues getContentValues();
    void setSelected(boolean selected);
}
