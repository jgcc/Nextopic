package com.itsa.nextopic.data.source;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import com.itsa.nextopic.data.source.local.DatabaseHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AssetDatabaseHelper {

    private Context context;

    public AssetDatabaseHelper(@NonNull Context context) {
        this.context = context;
    }

    public SQLiteDatabase openDatabase() {
        File dbFile = context.getDatabasePath(DatabaseHelper.DATABASE_NAME);
        if (!dbFile.exists()) {
            try {
                SQLiteDatabase checkDb = context.openOrCreateDatabase(DatabaseHelper.DATABASE_NAME, Context.MODE_PRIVATE, null);
                if (checkDb != null) {
                    checkDb.close();
                }
                copyDatabase(dbFile);
                Log.d(AssetDatabaseHelper.class.getSimpleName(), "database moved.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return DatabaseHelper.getInstance(context).getReadableDatabase();
    }

    private void copyDatabase(File dbFile) throws IOException {
        InputStream inputStream = context.getAssets().open(DatabaseHelper.DATABASE_NAME);
        OutputStream outputStream = new FileOutputStream(dbFile);

        byte[] buffer = new byte[1024];
        while (inputStream.read(buffer) > 0) {
            outputStream.write(buffer);
        }

        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }
}
