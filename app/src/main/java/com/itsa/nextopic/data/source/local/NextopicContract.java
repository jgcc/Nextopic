package com.itsa.nextopic.data.source.local;

/*
 * MIT License
 * Copyright (c) 2018 jose carabez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Project: Nextopic
 * Created by jose carabez
 * on 29/12/17.
 */

public final class NextopicContract {

    public static final String CONTENT_AUTHORITY = "com.itsa.nextopic.database";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final class TopicEntry implements BaseColumns {
        public static final String TABLE_NAME = "topics";
        public static final String COLUMN_TOPIC_NAME = "name";
        public static final String COLUMN_SUBJECT_ID = "subject_id";
        public static final String COLUMN_SCHEDULE_DATE = "schedule_date";
        public static final String COLUMN_REAL_DATE = "real_date";
        public static final String COLUMN_TURN = "turn";
        public static final String COLUMN_DELETED = "deleted";
        public static final String COLUMN_UUID = "uuid";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
        // These are special type prefixes that specify if a URI returns a list or a specific item
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_URI  + "/" + TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_URI + "/" + TABLE_NAME;


        public static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                " ("+_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                COLUMN_TOPIC_NAME + " TEXT," +
                COLUMN_SUBJECT_ID + " INTEGER," +
                COLUMN_SCHEDULE_DATE + " TEXT," +
                COLUMN_REAL_DATE + " TEXT," +
                COLUMN_TURN + " INTEGER," +
                COLUMN_DELETED + " INTEGER," +
                COLUMN_UUID + " TEXT )";

        public static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static Uri buildTopicUri() {
            return CONTENT_URI.buildUpon().build();
        }

        public static Uri buildTopicUriWith(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class SubjectEntry implements BaseColumns {
        public static final String TABLE_NAME = "subjects";
        public static final String COLUMN_SUBJECTS_NAME = "materia";
        public static final String COLUMN_SUBJECTS_KEY = "clave_materia";
        public static final String COLUMN_SUBJECTS_CREDITS = "creditos";
        public static final String COLUMN_COLOR = "color";
        public static final String COLUMN_SUBJECTS_HT = "ht";
        public static final String COLUMN_SUBJECTS_HP = "hp";
        public static final String COLUMN_SUBJECTS_SEMESTER_ID = "semester_id";
        public static final String COLUMN_SUBJECTS_CAREER_ID = "career_id";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
        // These are special type prefixes that specify if a URI returns a list or a specific item
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_URI  + "/" + TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_URI + "/" + TABLE_NAME;


        public static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                " ("+_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                COLUMN_SUBJECTS_NAME + " TEXT," +
                COLUMN_SUBJECTS_KEY + " TEXT," +
                COLUMN_SUBJECTS_CREDITS + " INTEGER," +
                COLUMN_SUBJECTS_HT + " INTEGER," +
                COLUMN_SUBJECTS_HP + " INTEGER," +
                COLUMN_SUBJECTS_SEMESTER_ID + " INTEGER," +
                COLUMN_SUBJECTS_CAREER_ID + " INTEGER," +
                COLUMN_COLOR + " TEXT)";

        public static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static Uri buildTopicUri() {
            return CONTENT_URI.buildUpon().build();
        }

        public static Uri buildTopicUriWith(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static class ReminderEntry implements BaseColumns {
        public static final String TABLE_NAME = "reminders";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_NOTES = "notes";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_FINISHED = "finished";
        public static final String COLUMN_NAME_DELETED = "deleted";
        public static final String COLUMN_NAME_UUID = "uuid";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
        // These are special type prefixes that specify if a URI returns a list or a specific item
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_URI  + "/" + TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_URI + "/" + TABLE_NAME;

        public static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                " ("+_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                COLUMN_NAME_TITLE + " TEXT," +
                COLUMN_NAME_NOTES + " TEXT," +
                COLUMN_NAME_DATE + " TEXT," +
                COLUMN_NAME_TIME + " TEXT," +
                COLUMN_NAME_FINISHED + " INTEGER," +
                COLUMN_NAME_DELETED + " INTEGER," +
                COLUMN_NAME_UUID + " TEXT )";

        public static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static Uri buildTopicUri() {
            return CONTENT_URI.buildUpon().build();
        }

        public static Uri buildTopicUriWith(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static class ScheduleEntry implements BaseColumns {
        public static final String TABLE_NAME = "schedule";
        public static final String COLUMN_ID_SUBJECT = "id_subject";
        public static final String COLUMN_DAY = "day";
        public static final String COLUMN_TIME_START = "time_start";
        public static final String COLUMN_TIME_FINISH = "time_finish";
        public static final String COLUMN_DELETED = "deleted";
        public static final String COLUMN_UUID = "uuid";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
        // These are special type prefixes that specify if a URI returns a list or a specific item
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_URI  + "/" + TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_URI + "/" + TABLE_NAME;

        public static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                " ("+_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                COLUMN_ID_SUBJECT + " INTEGER," +
                COLUMN_DAY + " INTEGER," +
                COLUMN_TIME_START + " TEXT," +
                COLUMN_TIME_FINISH + " TEXT," +
                COLUMN_DELETED + " INTEGER," +
                COLUMN_UUID + " TEXT )";

        public static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static Uri buildUri() {
            return CONTENT_URI.buildUpon().build();
        }

        public static Uri buildUriWith(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
